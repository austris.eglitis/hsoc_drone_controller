# General project description
This is a thesis project of Austris Eglitis - _Drone Control Using Heterogenous FPGA-based System on Chip_.
Info about the _ZynqBerryZero HSoC_
used in this project can be found [here](https://shop.trenz-electronic.de/en/TE0727-02-41C34-ZynqBerryZero-Module-with-Xilinx-Zynq-7010-512-MB-DDR3L-SDRAM-3-x-6.5-cm?path=Trenz_Electronic/Modules_and_Module_Carriers/3.05x6.5/TE0727/Reference_Design/2019.2/zbzerodemo1).


Project holds both HW and SW directories. Project structure of relavant files can be found below.


General structure of **sw** directory:
```
sw
├── esc_controller                  
├── motor_lift_force_measurements
├── petalinux_config_files
└── tcpip_communication
    ├── device
    └── host

```
sw direcotory holds software components used in the project.
*  ***esc\_controller***         Holds program for accessing /dev/mem and sending commands to drone motors. Uses mmap() function.
* ***motor\_lift\_force***       Holds arduino code for taking load cell measurements used for motor lift force.
* ***petalinux\_config\_files*** Holds generated configuration files of petalinux project. Conf files hold information about rootfs, kernel and other settings. 
* ***tcpip\_communication***     Holds programm for TCP/IP communication. Programm is configured to send data between 2 processes inside one device, hence the IP is set to 127.0.0.1, but can be changed accordingly for communication between 2 processes in 2 different devices.



General structure of **hw** directory:

```
hw
├── xilinx 
├── edi.ip 
└── components 
    ├── crc
    ├── crc_first
    ├── drone_regmap 
    ├── dshot_frame_packer 
    ├── dshot_frame_sender
    └── drone_controller 
```


hw direcotory holds hardware components used in the project.
* ***xilinx***       Holds _Trenz Electronics_ provided Xilinx Vivado template project for _ZynqBerry Zero_. Template project holds BSP and generated Vitis applications. This direcotry holds 2 relavant software applications used in Vitis for BOOT.mcs generation and QSPI memory flash.

These applications have been mentioned in thesis:
```
    xilinx/vivado/sw_lib/sw_apps/zynq_fsbl
    xilinx/vivado/sw_lib/sw_apps/zynq_fsbl_flash
```

* ***edi.ip***       Holds EDI provided libraries, e.g., AXI block.
* ***components***   Holds VHDL components which are mentioned in thesis. 
