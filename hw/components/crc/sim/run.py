#!/usr/bin/env python3
import edi_ic_routines as edi

# set up Vunit project and dependencies
[prj, tbLib] = edi.vunit_setupProjectAddDeps("../src/*.vhd", "../tb/*.vhd")

# set up test configurations
tbLib.test_bench("tb").test("full_coverage").add_config(
  name="crc_length=2,data_length=4",
  generics=dict(
    CRC_LENGTH  = 2,
    DATA_LENGTH = 4))

tbLib.test_bench("tb").test("full_coverage").add_config(
  name="crc_length=2,data_length=8",
  generics=dict(
    CRC_LENGTH  = 2,
    DATA_LENGTH = 8))

tbLib.test_bench("tb").test("full_coverage").add_config(
  name="crc_length=4,data_length=8",
  generics=dict(
    CRC_LENGTH  = 4,
    DATA_LENGTH = 8))


# Run vunit function
prj.main()
