---------------------------------------------------------------------------
--! @file crc.vhd
--! @author TODO
---------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library edi_dsp;
use edi_dsp.functions.all;
  
entity crc is
  port(
   clk      : in  std_logic;
   i_valid    : in  std_logic;        --! input stream, input data is valid
   i_polynome    : in  std_logic_vector; --! input stream, polynome
   i_data      : in  std_logic_vector; --! input stream, data to crced
   o_ready    : out std_logic;        --! input stream, ready to receive data
   o_valid        : out std_logic;        --! output stream, output data valid
   o_crc      : out std_logic_vector; --! output stream, output crc
   i_ready    : in  std_logic         --! output stream, next component ready
  );
end entity;

architecture RTL of crc is
  constant DATA_WIDTH      : natural := i_data'length;
  constant VALID_WIDTH     : natural := DATA_WIDTH+1;
  constant POLYNOME_WIDTH  : natural := i_polynome'length;
  constant CRC_WIDTH       : natural := o_crc'length;
  constant DATA_ZEROS      : std_logic_vector(CRC_WIDTH-1 downto 0) := (others => '0');
  
  signal data_reg, data_next, data_next_xor  : std_logic_vector(DATA_WIDTH+CRC_WIDTH-1 downto 0) := (others=>'0');
  signal polynome_reg, polynome_next         : std_logic_vector(POLYNOME_WIDTH-1 downto 0):= (others=>'0');
  signal valid_reg, valid_next               : std_logic_vector(VALID_WIDTH-1 downto 0) := (others=>'0');
  signal internal_en, start                  : std_logic;
begin

  -- reg state logic
  process(clk)
  begin 
    if rising_edge(clk) and internal_en = '1' then
      data_reg     <= data_next;
      polynome_reg <= polynome_next;
      valid_reg    <= valid_next;
    end if;
  end process;

  --next state logic
  VALIDS: for i in 1 to valid_reg'high generate
    valid_next(i) <= valid_reg(i-1);
  end generate;
  valid_next(0) <= start;

  polynome_next <= i_polynome when start = '1' else polynome_reg;

  data_next <= i_data & DATA_ZEROS when start = '1' else
               data_next_xor       when data_reg(data_reg'high) = polynome_reg(polynome_reg'high) else
               data_reg(data_reg'high-1 downto 0) & '0'; 

  XORING: for i in 0 to CRC_WIDTH-1 generate
    data_next_xor(data_next_xor'high-i) <= data_reg(data_reg'high-i-1) XOR polynome_reg(polynome_reg'high-i-1);
  end generate;
  data_next_xor(data_next_xor'high-CRC_WIDTH downto 0) <= data_reg(data_reg'high-CRC_WIDTH-1 downto 0) & "0";

  start <= i_valid AND NOT generic_or(valid_reg(valid_reg'high-1 downto 0));

  internal_en <= not valid_reg(valid_reg'high) or i_ready;

  -- output
  o_crc     <= data_reg(data_reg'length-1 downto data_reg'length-CRC_WIDTH);
  o_valid   <= valid_reg(valid_reg'high);
  o_ready   <= internal_en; 
end architecture;
