library ieee;
library edi_dsp;
use ieee.std_logic_1164.all;

entity main is
  port(
    clk       : in  std_logic;
    arst      : in  std_logic;
    -- input interface
    i_start   : in  std_logic;
    i_polynom : in  std_logic_vector(4 downto 0);
    i_data    : in  std_logic_vector(11 downto 0);
    o_ready   : out std_logic;
    -- output interface
    o_valid   : out std_logic;
    o_crc     : out std_logic_vector(3 downto 0)
  );
end entity;

architecture RTL of main is
begin

  COMP : entity work.crc
  port map(
    clk        => clk,
--    arst       => open,
    i_valid    => i_start,
    i_ready    => '1',
    i_polynome => i_polynom,
    i_data     => i_data,
    o_ready    => o_ready,
    o_valid    => o_valid,
    o_crc      => o_crc);

end architecture;
