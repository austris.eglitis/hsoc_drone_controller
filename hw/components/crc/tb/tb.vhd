library std;
library ieee;
library uvvm_util;
library vunit_lib;
library osvvm;
library edi;
library edi_dsp;

context uvvm_util.uvvm_util_context;
context vunit_lib.vunit_context;
context vunit_lib.vc_context;

use std.env.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_pkg.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use vunit_lib.memory_pkg.all;
use vunit_lib.axi_stream_pkg.all;
use vunit_lib.stream_slave_pkg.all;
use vunit_lib.stream_master_pkg.all;

use edi.data_types.all;


entity tb is
  generic(
    runner_cfg   : string;
    CRC_LENGTH   : natural := 3;
    DATA_LENGTH  : natural := 8
  );
end entity;


architecture RTL of tb is
  constant POLY_LENGTH : natural := CRC_LENGTH+1;
  constant CRC_HIGH    : natural := CRC_LENGTH-1;
  constant POLY_HIGH   : natural := POLY_LENGTH-1;
  constant TEST_COUNT  : natural := 2**(DATA_LENGTH+POLY_LENGTH-1);
  constant TIMEOUT     : time    := (DATA_LENGTH+3)*10 ns;

  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
   signal clk       : sl := '0';
   signal arst      : sl := '0';
   signal i_start   : sl := '0';
   signal i_polynom : slv(POLY_LENGTH-1 downto 0) := (others => '0');
   signal i_data    : slv(DATA_LENGTH-1 downto 0) := (others => '0');
   signal o_ready   : sl := '0';
   signal o_valid   : sl := '0';
   signal o_crc     : slv(CRC_LENGTH-1 downto 0) := (others => '0');

  -----------------------------------------------------------------------------
  -- Clock related
  -----------------------------------------------------------------------------
  constant CLK_PERIOD : time    := 10 ns;
  signal clk_en       : boolean := true;

  -----------------------------------------------------------------------------
  -- Verification components
  -----------------------------------------------------------------------------
  constant memory : memory_t := new_memory;

begin

  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity edi_dsp.crc
  port map(
    clk        => clk,
    --arst       => open,
    i_valid    => i_start,
    i_ready    => '1',
    i_polynome => i_polynom,
    i_data     => i_data,
    o_ready    => o_ready,
    o_valid    => o_valid,
    o_crc      => o_crc);

  -----------------------------------------------------------------------------
  -- Clock instantation
  -----------------------------------------------------------------------------
  clock_generator(clk, clk_en, CLK_PERIOD, "TB clock");

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    ---------------------------------------------------------------------------
    -- Functions
    ---------------------------------------------------------------------------
    function calculate_crc(v_data, v_poly: slv) return slv is
      constant CRC_VECTOR_LENGTH : natural := DATA_LENGTH+CRC_LENGTH;
      variable v_vector : slv(CRC_VECTOR_LENGTH-1 downto 0) := (others => '0');
    begin
      -- put data in the CRC calculation vector
      v_vector(v_vector'high downto v_vector'high-v_data'length+1) := v_data;

      -- CRC algorithm
      for i in v_vector'high downto v_poly'high loop
        -- skip to next iteration as per algorithm
        if v_vector(i) = '0' then
          next;
        end if;

        -- xor with polynom starting from the highest '1' bit
        v_vector(i downto i-v_poly'length+1)
          := v_vector(i downto i-v_poly'length+1) xor v_poly;
      end loop;

      -- uncomment for function debugging
      --info("poly:"   & to_string(v_poly) 
      --  & "; data: " & to_string(v_data)
      --  & "; crc: "  & to_string(v_vector(CRC_LENGTH-1 downto 0)));
      return v_vector(CRC_LENGTH-1 downto 0);
    end function;

    ---------------------------------------------------------------------------
    -- Procedures
    ---------------------------------------------------------------------------
    procedure dut_push_and_set_expected_full_coverage(buf: inout buffer_t) is
      variable v_combined : slv(POLY_LENGTH+DATA_LENGTH-2 downto 0);
      variable v_data : slv(DATA_LENGTH-1 downto 0);
      variable v_poly : slv(POLY_LENGTH-1 downto 0);
      variable v_crc  : slv(CRC_LENGTH-1 downto 0);
    begin
      v_poly(v_poly'high) := '1';
      for i in 0 to TEST_COUNT-1 loop
        -- generate data / poly vectors for full coverage
        v_combined
          := slv(to_unsigned(i, v_combined'length));
        v_poly(POLY_LENGTH-2 downto 0)
          := v_combined(v_combined'high downto DATA_LENGTH);
        v_data 
          := v_combined(DATA_LENGTH-1 downto 0);

        -- calculate crc and set expected
        v_crc := calculate_crc(v_data, v_poly);
        set_expected_byte(memory, base_address(buf)+i,
          to_integer(unsigned(v_crc)));
      end loop;
    end procedure;

    procedure dut_pop_full_coverage(buf : inout buffer_t) is
      variable v_combined : slv(POLY_LENGTH+DATA_LENGTH-2 downto 0);
      variable v_data : slv(DATA_LENGTH-1 downto 0);
      variable v_poly : slv(POLY_LENGTH-1 downto 0);
      variable v_expected : slv(CRC_LENGTH-1 downto 0);
    begin
      -- polynom's highest bit must always be '1'
      v_poly(v_poly'high) := '1';

      for i in 0 to TEST_COUNT-1 loop
        -- generate data / poly vectors for full coverage
        v_combined
          := slv(to_unsigned(i, v_combined'length));
        v_poly(POLY_LENGTH-2 downto 0)
          := v_combined(v_combined'high downto DATA_LENGTH);
        v_data 
          := v_combined(DATA_LENGTH-1 downto 0);

        -- wait for the crc component to become ready
        wait until o_ready = '1' for TIMEOUT;
        check(o_ready = '1', "Expecting CRC to become ready");
         
        -- apply input signals
        wait until falling_edge(clk);
        i_start   <= '1';
        i_polynom <= v_poly;
        i_data    <= v_data;
        wait until falling_edge(clk);
        i_start   <= '0';


        -- wait for outputs to become available
        wait until o_valid = '1' for TIMEOUT;
        check(o_valid = '1', "Expecting output data become valid");

	    -- retreive expected data
        v_expected := slv(to_unsigned(
          get_expected_byte(memory, base_address(buf)+i), o_crc'length));

        -- prepare messages for easier debbugging
        info(
          "poly: "     & to_string(v_poly)     & "; " &
          "data: "     & to_string(v_data)     & "; " &
          "expected: " & to_string(v_expected) & "; " &
          "got: "      & to_string(o_crc));

        -- write output to the memory
        write_byte(memory, base_address(buf)+i, to_integer(unsigned(o_crc)));
      end loop;
    end procedure;

    ---------------------------------------------------------------------------
    -- Variables
    ---------------------------------------------------------------------------
    variable buf : buffer_t;
  begin
    test_runner_setup(runner, runner_cfg);
    gen_pulse(arst, 1*CLK_PERIOD, "Activated reset for 1 period");

    while test_suite loop
      if run("full_coverage") then
        info("Allocating expected buffer");
        buf := allocate(memory, num_bytes => 4*TEST_COUNT,
          name=>buf'simple_name, permissions => write_only, alignment => 4096);

        info("Initializing expected memory data");
        dut_push_and_set_expected_full_coverage(buf);

        info("Running test");
        dut_pop_full_coverage(buf);
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;

end architecture;
