---------------------------------------------------------------------------
--! @file crc.vhd
--! @author Austris 
---------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library edi;
	
entity crc is
  port(
	 clk			: in  std_logic;
	 i_valid		: in  std_logic;        --! input stream, input data is valid
	 i_polynome		: in  std_logic_vector(4 downto 0); --! input stream, polynome
	 i_data			: in  std_logic_vector(11 downto 0); --! input stream, data to crced
	 o_ready		: out std_logic;        --! input stream, ready to receive data
	 o_valid        : out std_logic;        --! output stream, output data valid
	 o_crc			: out std_logic_vector(3 downto  0); --! output stream, output crc
	 i_ready		: in  std_logic         --! output stream, next component ready
  );
end entity;

architecture RTL of crc is
  constant DATA_WIDTH      : natural := i_data'length;
  constant POLYNOME_WIDTH  : natural := i_polynome'length;
  constant CRC_WIDTH       : natural := o_crc'length;
  
  signal data_reg, data_next 			 : std_logic_vector(DATA_WIDTH-1 + CRC_WIDTH downto 0);
  signal polynome_reg, polynome_next	 : std_logic_vector (POLYNOME_WIDTH-1 downto 0);
  signal valid_reg, valid_next 			 : std_logic;
  signal ready_reg, ready_next 			 : std_logic := '1';
  signal counter_reg, counter_next 		 : unsigned (DATA_WIDTH-1 downto 0) := (others => '0'); --(log2(DATA_WIDTH-1)+1 downto 0)
	  
	begin	
		process(clk)
			variable empty_crc : std_logic_vector (CRC_WIDTH-1 downto 0) := (others => '0');
		begin 
			 if clk'event and clk='1' then
				if (i_valid='1') and (ready_reg='1') and (i_ready='1') then
					data_reg <= i_data & empty_crc; --data & "0000"
					ready_reg <= ready_next;
					valid_reg <= valid_next;
					counter_reg <= (others=>'0'); -- counter reset
					polynome_reg <= i_polynome;
				else
					data_reg <= data_next;
					ready_reg <= ready_next;
					valid_reg <= valid_next;
					counter_reg <= counter_next;
					polynome_reg <= polynome_next;
				end if;
			end if;
		end process;

		process(counter_reg, counter_next, ready_reg, ready_next, valid_reg, valid_next, polynome_reg, polynome_next, data_reg, data_next)
			variable data_tmp : std_logic_vector (POLYNOME_WIDTH-1 downto 0) := (others=>'0');
			begin
				polynome_next <= polynome_reg;
				if (counter_reg=data_reg'length - POLYNOME_WIDTH+1) then
					counter_next <= counter_reg+1;
					ready_next <= '1';
					valid_next <= '1';
					--last CRC iteration:
					if (data_reg(data_reg'length-1)='0') then --ja MSB data_reg ir 0, tad
						data_next <= data_reg(data_reg'length-2 downto 0) & '0';
					else 
						for i in POLYNOME_WIDTH-1 downto 0 loop
							data_tmp(data_tmp'length-1-i) := data_reg(data_reg'length-1-i) XOR polynome_reg(i);
						end loop;
						data_next <= data_tmp(data_tmp'length-2 downto 0) & data_reg(data_reg'length-1-POLYNOME_WIDTH downto 0) & '0';
					end if;
				elsif (counter_reg=data_reg'length - POLYNOME_WIDTH +2) then --waiting until i_ready and i_valid is 1
					counter_next <= counter_reg;
					ready_next <= '1';
					valid_next <= '1';
					data_next <=data_reg;		
				else
					counter_next <= counter_reg +1;
					ready_next <= '0';
					valid_next <= '0';
					--CRC operation:
					if (data_reg(data_reg'length-1)='0') then
						data_next <= data_reg(data_reg'length-2 downto 0) & '0';
					else 
						for i in POLYNOME_WIDTH-1 downto 0 loop
							data_tmp(data_tmp'length-1-i) := data_reg(data_reg'length-1-i) XOR polynome_reg(i);
						end loop;
						data_next <= data_tmp(data_tmp'length-2 downto 0) & data_reg(data_reg'length-1-POLYNOME_WIDTH downto 0) & '0';
					end if;
				end if;
		end process;
		
	 -- output
	o_crc 		<= data_reg(data_reg'length-1 downto data_reg'length - CRC_WIDTH);
	o_valid		<= valid_reg;
	o_ready 	<= ready_reg AND i_ready; 
end architecture;
