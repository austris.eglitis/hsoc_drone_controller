action   = "simulation"
sim_tool = "modelsim"
vcom_opt = "-2008"
sim_top  = "tb"

files = [
  "../src/crc.vhd",
  "../tb/tb.vhd",
]

modules = {
  "local" : [
    "../../../../../../../../usr/local/src/edi_ic_design_ip"
  ]
}