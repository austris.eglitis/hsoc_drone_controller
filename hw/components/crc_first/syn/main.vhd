library ieee;
library edi;
use ieee.std_logic_1164.all;

entity main is
  port(
    clk        : in  std_logic;
    i_valid    : in  std_logic;
    i_polynome : in  std_logic_vector(4 downto 0);
    i_data     : in  std_logic_vector(11 downto 0);
    o_ready    : out std_logic;
    o_valid    : out std_logic;
    o_crc      : out std_logic_vector(3 downto 0);
    i_ready    : in  std_logic
  );
end entity;

architecture RTL of main is
begin

  COMP: entity work.crc
  port map(
    clk        => clk,
    i_valid    => i_valid,
    i_polynome => i_polynome,
    i_data     => i_data,
    o_ready    => o_ready,
    o_valid    => o_valid,
    o_crc      => o_crc,
    i_ready    => i_ready
  );

end architecture;
