library std;
library ieee;
library uvvm_util;
library vunit_lib;
library osvvm;
library edi;

context uvvm_util.uvvm_util_context;
context vunit_lib.vunit_context;
context vunit_lib.vc_context;

use std.env.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_pkg.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use vunit_lib.memory_pkg.all;
use vunit_lib.axi_stream_pkg.all;
use vunit_lib.stream_slave_pkg.all;
use vunit_lib.stream_master_pkg.all;


entity tb is
  generic(
    runner_cfg : string
  );
end entity;


architecture RTL of tb is
  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------

  -----------------------------------------------------------------------------
  -- Clock related
  -----------------------------------------------------------------------------
  constant CLK_PERIOD : time := 10 ns;

  -----------------------------------------------------------------------------
  -- Verification components
  -----------------------------------------------------------------------------

begin

  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity edi.crc
  generic map(
  )
  port map(
  );

  -----------------------------------------------------------------------------
  -- Clock instantation
  -----------------------------------------------------------------------------
  clock_generator(clk, true, CLK_PERIOD, "TB clock");

  ----------------------------------------------------------------------------
  -- Verification component instantiation
  ----------------------------------------------------------------------------

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    ---------------------------------------------------------------------------
    -- Variables
    ---------------------------------------------------------------------------

    ---------------------------------------------------------------------------
    -- Procedures
    ---------------------------------------------------------------------------

  begin
    test_runner_setup(runner, runner_cfg);
    gen_pulse(clk, CLK_PERIOD, "Activated reset for 1 period");

    while test_suite loop
      if run("Full coverage") then
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;

end architecture;
