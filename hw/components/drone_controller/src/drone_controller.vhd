library ieee;
library edi;
library edi_interface;
use ieee.std_logic_1164.all;
use edi.data_types.all; 
use edi.functions.all; 

entity drone_controller is
  generic(
    BIT_PERIOD_CLOCK_COUNT : natural := 1333
  );
  port(
    i_aclk    : in std_logic;
    i_areset  : in std_logic;

    -- memory mapped interface
    i_axi_awvalid : in  std_logic;
    o_axi_awready : out std_logic;
    i_axi_awaddr  : in  std_logic_vector;
    i_axi_wvalid  : in  std_logic;
    o_axi_wready  : out std_logic;
    i_axi_wdata   : in  std_logic_vector;
    i_axi_wstrb   : in  std_logic_vector;
    o_axi_bvalid  : out std_logic;
    i_axi_bready  : in  std_logic;
    o_axi_bresp   : out std_logic_vector;
    i_axi_arvalid : in  std_logic;
    o_axi_arready : out std_logic;
    i_axi_araddr  : in  std_logic_vector;
    o_axi_rvalid  : out std_logic;
    i_axi_rready  : in  std_logic;
    o_axi_rdata   : out std_logic_vector;
    o_axi_rresp   : out std_logic_vector;

    -- memory mapped interface
    o_esc_fl : out std_logic;
    o_esc_fr : out std_logic;
    o_esc_bl : out std_logic;
    o_esc_br : out std_logic
  );
end entity;

architecture RTL of drone_controller is
  signal o_esc_frame_fr, o_esc_frame_fl, o_esc_frame_br, o_esc_frame_bl : std_logic_vector(31 downto 0); 
  signal o_esc_delay_fr, o_esc_delay_fl, o_esc_delay_br, o_esc_delay_bl : std_logic_vector(31 downto 0);
  signal o_esc_enable_fr, o_esc_enable_fl,o_esc_enable_br, o_esc_enable_bl : std_logic;

  signal i_reg_en, i_reg_input, o_reg_output : aslv(0 to 11)(31 downto 0);

begin



  AXI_LITE_SLAVE: entity edi.axi_lite_slave
  port map(
    o_reg_input   => i_reg_input,
    i_reg_output  => o_reg_output,
    o_reg_en      => i_reg_en,

    i_axi_aclk    => i_aclk,
    i_axi_areset  => i_areset,

    i_axi_awvalid => i_axi_awvalid,
    o_axi_awready => o_axi_awready,
    i_axi_awaddr  => i_axi_awaddr,
    i_axi_wvalid  => i_axi_wvalid,
    o_axi_wready  => o_axi_wready,
    i_axi_wdata   => i_axi_wdata,
    i_axi_wstrb   => i_axi_wstrb,
    o_axi_bvalid  => o_axi_bvalid,
    i_axi_bready  => i_axi_bready,
    o_axi_bresp   => o_axi_bresp,

    i_axi_arvalid => i_axi_arvalid,
    o_axi_arready => o_axi_arready,
    i_axi_araddr  => i_axi_araddr,
    o_axi_rvalid  => o_axi_rvalid,
    i_axi_rready  => i_axi_rready,
    o_axi_rdata   => o_axi_rdata,
    o_axi_rresp   => o_axi_rresp );

  REGMAP : entity work.drone_regmap
  port map(
    i_clk        => i_aclk,
    i_reg_en     => i_reg_en,
    i_reg_input  => i_reg_input,
    o_reg_output => o_reg_output,

    o_esc_frame_fr  => o_esc_frame_fr(15 downto 0), 
    o_esc_delay_fr  => o_esc_delay_fr(15 downto 0),
    o_esc_enable_fr => o_esc_enable_fr,

    o_esc_frame_fl  => o_esc_frame_fl(15 downto 0),
    o_esc_delay_fl  => o_esc_delay_fl(15 downto 0),
    o_esc_enable_fl => o_esc_enable_fl,

    o_esc_frame_br  => o_esc_frame_br(15 downto 0),
    o_esc_delay_br  => o_esc_delay_br(15 downto 0),
    o_esc_enable_br => o_esc_enable_br,

    o_esc_frame_bl => o_esc_frame_bl(15 downto 0),
    o_esc_delay_bl => o_esc_delay_bl(15 downto 0),
    o_esc_enable_bl => o_esc_enable_bl );

  ESC_SENDER_FL: entity edi_interface.dshot_frame_sender
   generic map(
    BIT_PERIOD_CLOCK_COUNT => BIT_PERIOD_CLOCK_COUNT
   )
   port map (
     clk      => i_aclk,
     i_enable => o_esc_enable_fl,
     i_frame  => o_esc_frame_fl(15 downto 0),
     i_delay  => o_esc_delay_fl(15 downto 0),
     o_dshot  => o_esc_fl,
     o_done   => open );

 ESC_SENDER_FR: entity edi_interface.dshot_frame_sender
  generic map(
    BIT_PERIOD_CLOCK_COUNT => BIT_PERIOD_CLOCK_COUNT
  )
  port map(
    clk      => i_aclk,
    i_enable => o_esc_enable_fr,
    i_frame  => o_esc_frame_fr(15 downto 0),
    i_delay  => o_esc_delay_fr(15 downto 0),
    o_dshot  => o_esc_fr,
    o_done   => open );

 ESC_SENDER_BL: entity edi_interface.dshot_frame_sender
  generic map(
    BIT_PERIOD_CLOCK_COUNT => BIT_PERIOD_CLOCK_COUNT 
  )
  port map(
    clk      => i_aclk,
    i_enable => o_esc_enable_bl, 
    i_frame  => o_esc_frame_bl(15 downto 0),
    i_delay  => o_esc_delay_bl(15 downto 0),
    o_dshot  => o_esc_bl,
    o_done   => open );

 ESC_SENDER_BR: entity edi_interface.dshot_frame_sender
  generic map(
    BIT_PERIOD_CLOCK_COUNT => BIT_PERIOD_CLOCK_COUNT
  )
  port map(
    clk      => i_aclk,
    i_enable => o_esc_enable_br, 
    i_frame  => o_esc_frame_br(15 downto 0),
    i_delay  => o_esc_delay_br(15 downto 0),
    o_dshot  => o_esc_br,
    o_done   => open );

end architecture;

