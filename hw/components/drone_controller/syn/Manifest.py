action = "synthesis"
target = "altera"

syn_family  = "Cyclone V"
syn_device  = "5csema5"
syn_package = "f31"
syn_grade   = "c6"
syn_top     = "main"
syn_project = "main"
syn_tool    = "quartus"

syn_properties = [
  {"name": "VHDL_INPUT_VERSION", "value": "VHDL_2008"},
  {"name": "VHDL_FILE", "value": "../../../edi_ip/libs/ieee/fixed_float_types.vhd"},
  {"name": "VHDL_FILE", "value": "../../../edi_ip/libs/ieee/fixed_pkg.vhd"},
]

files = [
  "../src/drone_controller.vhd",
  "./main.vhd",
]

modules = {
  "local" : [
    "../../../edi_ip",
  ]
}