library ieee;
library edi;
use ieee.std_logic_1164.all;
use edi.data_types.all; 
use edi.functions.all; 


entity main is
  port(
    i_aclk    : in sl;
    i_areset  : in sl;

    -- memory mapped interface
    i_axi_awvalid : in  sl;
    o_axi_awready : out sl;
    i_axi_awaddr  : in  slv(5 downto 0);
    i_axi_wvalid  : in  sl;
    o_axi_wready  : out sl;
    i_axi_wdata   : in  slv(31 downto 0);
    i_axi_wstrb   : in  slv(3 downto 0);
    o_axi_bvalid  : out sl;
    i_axi_bready  : in  sl;
    o_axi_bresp   : out slv(1 downto 0);
    i_axi_arvalid : in  sl;
    o_axi_arready : out sl;
    i_axi_araddr  : in  slv(5 downto 0);
    o_axi_rvalid  : out sl;
    i_axi_rready  : in  sl;
    o_axi_rdata   : out slv(31 downto 0);
    o_axi_rresp   : out slv(1 downto 0);

    -- memory mapped interface
    o_esc_fl : out sl;
    o_esc_fr : out sl;
    o_esc_bl : out sl;
    o_esc_br : out sl
  );
end entity;

architecture rtl of main is
begin

  DRONE_CTRL : entity work.drone_controller 
  port map(
    i_aclk    => i_aclk,
    i_areset  => i_areset,

    i_axi_awvalid => i_axi_awvalid,
    o_axi_awready => o_axi_awready,
    i_axi_awaddr  => i_axi_awaddr,
    i_axi_wvalid  => i_axi_wvalid,
    o_axi_wready  => o_axi_wready,
    i_axi_wdata   => i_axi_wdata,
    i_axi_wstrb   => i_axi_wstrb,
    o_axi_bvalid  => o_axi_bvalid,
    i_axi_bready  => i_axi_bready,
    o_axi_bresp   => o_axi_bresp,

    i_axi_arvalid => i_axi_arvalid,
    o_axi_arready => o_axi_arready,
    i_axi_araddr  => i_axi_araddr,
    o_axi_rvalid  => o_axi_rvalid,
    i_axi_rready  => i_axi_rready,
    o_axi_rdata   => o_axi_rdata,
    o_axi_rresp   => o_axi_rresp,

    o_esc_fl => o_esc_fl,
    o_esc_fr => o_esc_fr,
    o_esc_bl => o_esc_bl,
    o_esc_br => o_esc_br
  );




end architecture;
