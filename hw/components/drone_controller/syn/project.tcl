load_package flow
project_new main
remove_all_global_assignments -name *_FILE
source files.tcl
set_global_assignment -name FAMILY "Cyclone V"
set_global_assignment -name DEVICE "5csema5f31c6"
set_global_assignment -name TOP_LEVEL_ENTITY "main"
set_global_assignment -name VHDL_INPUT_VERSION "VHDL_2008"
set_global_assignment -name VHDL_FILE "../../../edi_ip/libs/ieee/fixed_float_types.vhd"
set_global_assignment -name VHDL_FILE "../../../edi_ip/libs/ieee/fixed_pkg.vhd"
