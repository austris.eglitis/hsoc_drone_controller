library ieee;
library edi;
use ieee.std_logic_1164.all;
use edi.data_types.all; 
use edi.functions.all; 

entity drone_regmap is 
  port(
    i_clk        : in  sl;
    i_reg_en     : in  aslv;
    i_reg_input  : in  aslv;
    o_reg_output : out aslv;

    o_esc_frame_fr  : out std_logic_vector(15 downto 0);
    o_esc_delay_fr  : out std_logic_vector(15 downto 0);
    o_esc_enable_fr : out std_logic;

    o_esc_frame_fl  : out std_logic_vector(15 downto 0);
    o_esc_delay_fl  : out std_logic_vector(15 downto 0);
    o_esc_enable_fl : out std_logic;

    o_esc_frame_br  : out std_logic_vector(15 downto 0);
    o_esc_delay_br  : out std_logic_vector(15 downto 0);
    o_esc_enable_br : out std_logic;

    o_esc_frame_bl  : out std_logic_vector(15 downto 0);
    o_esc_delay_bl  : out std_logic_vector(15 downto 0);
    o_esc_enable_bl : out std_logic
  );
end entity;

architecture RTL of drone_regmap is
    signal reg_output : aslv(o_reg_output'range)(o_reg_output(0)'range) := (others => (others => '0'));
begin
  -- reg-state
  process(i_clk)
  begin
    if rising_edge(i_clk) then
      for i in i_reg_input'range loop
      for j in i_reg_input(0)'range loop
        if i_reg_en(i)(j)='1' then
          reg_output(i)(j) <= i_reg_input(i)(j);
         end if;
      end loop;
      end loop;
    end if;
  end process;
  
  o_reg_output <= reg_output;

  --ESC 1
  o_esc_frame_fr  <= reg_output(0)(o_esc_frame_fr'range);
  o_esc_delay_fr  <= reg_output(1)(o_esc_delay_fr'range);
  o_esc_enable_fr <= reg_output(2)(0);  

  --ESC 2
  o_esc_frame_fl  <= reg_output(3)(o_esc_frame_fl'range);
  o_esc_delay_fl  <= reg_output(4)(o_esc_delay_fl'range);
  o_esc_enable_fl <= reg_output(5)(0);

  -- ESC 3
  o_esc_frame_br  <= reg_output(6)(o_esc_frame_br'range);
  o_esc_delay_br  <= reg_output(7)(o_esc_delay_br'range);
  o_esc_enable_br <= reg_output(8)(0);

  --ESC 4
  o_esc_frame_bl  <= reg_output(9)(o_esc_frame_bl'range);
  o_esc_delay_bl  <= reg_output(10)(o_esc_delay_bl'range);
  o_esc_enable_bl <= reg_output(11)(0);

end architecture;
