---------------------------------------------------------------------------
--! @file dshot_frame_packer.vhd
--! @author Austris 
---------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library edi_dsp;


entity dshot_frame_packer is
  port(
    clk : in std_logic;
    i_polynome : in std_logic_vector;
    i_data     : in std_logic_vector;
    i_ready    : in std_logic;
    i_valid    : in std_logic;
    o_ready    : out std_logic;
    o_frame    : out std_logic_vector
  );
end entity;

architecture RTL of dshot_frame_packer is
  constant DATA_WIDTH : natural := i_data'length;
  constant CRC_WIDTH  : natural := i_polynome'length-1;

  signal data_reg  : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
  signal frame_reg : std_logic_vector(DATA_WIDTH+CRC_WIDTH-1 downto 0) := (others => '0');
  signal crc_data  : std_logic_vector(CRC_WIDTH-1 downto 0);
  signal valid     : std_logic;
begin

  CRC: entity edi_dsp.crc
    port map(
     clk        => clk,
     i_valid    => i_valid,
     i_polynome => i_polynome,
     i_data     => i_data,
     o_ready    => o_ready,
     o_valid    => valid,
     o_crc      => crc_data,
     i_ready    => '1');


  -- reg-state logic
  process(clk)
  begin
    if rising_edge(clk) then
      if i_valid = '1' and o_ready = '1' then
        data_reg <= i_data;
      end if;
      if valid = '1' then
        frame_reg <=  data_reg & crc_data;
      end if;
   end if;
  end process;
 
  -- next-state logic

  -- outputs
  o_frame <= frame_reg;
end architecture;
