library ieee;
library edi_interface;
use ieee.std_logic_1164.all;

entity main is
  port(
    clk : in std_logic;
    i_polynome : in std_logic_vector(4 downto 0);
    i_data     : in std_logic_vector(11 downto 0);
    i_ready    : in std_logic;
    i_valid    : in std_logic;
    o_ready    : out std_logic;
    o_frame    : out std_logic_vector(15 downto 0)
  );
end entity;

architecture RTL of main is
begin

  COMP : entity work.dshot_frame_packer
  port map(
    clk        => clk,
    i_polynome => i_polynome,
    i_data     => i_data,
    i_ready    => i_ready,
    i_valid    => i_valid,
    o_ready    => o_ready,
    o_frame    => o_frame);

end architecture;
