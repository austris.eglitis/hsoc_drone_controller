---------------------------------------------------------------------------
--! @file dshot_frame_sender.vhd
--! @author Austris Eglitis 
---------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library edi;
use edi.functions.all;

--! @param BIT_PERIOD_CLOCK_COUNT Determines the length of a single bit transfer
--!        in clock periods. Can be used for switching between different dshot
--!        variants, i.e. dshot150, dshot300, dshot600.
entity dshot_frame_sender is
  generic(
    BIT_PERIOD_CLOCK_COUNT : natural := 1333
  );
  port(
    clk      : in  std_logic;                     --
    i_enable : in  std_logic;                     --
    i_frame  : in  std_logic_vector(15 downto 0); --
    i_delay  : in  std_logic_vector(15 downto 0); -- number of delay (reset) bits
    o_dshot  : out std_logic;                     --
    o_done   : out std_logic                      --
  );
end entity;

architecture RTL of dshot_frame_sender is
  constant BIT_LOW_CLOCK_COUNT  : natural := natural(0.35*real(BIT_PERIOD_CLOCK_COUNT));
  constant BIT_HIGH_CLOCK_COUNT : natural := natural(0.75*real(BIT_PERIOD_CLOCK_COUNT));

  signal period_counter_reg, period_counter_next : unsigned(i_delay'range) := (others => '0');
  signal clock_counter_reg, clock_counter_next   : unsigned(log2c(BIT_PERIOD_CLOCK_COUNT)-1 downto 0) := (others => '0');
  signal shift_reg, shift_next : std_logic_vector(i_frame'length-1 downto 0) := (others => '0');
  signal dshot_reg, dshot_next : std_logic := '0';
  signal done_reg, done_next   : std_logic := '0';

  signal max_period, max_clock : boolean;
  signal dshot_high, dshot_low : std_logic;

begin
  -- reg-state logic
  process(clk)
  begin
    if rising_edge(clk) then
      if i_enable='1' then
        clock_counter_reg  <= clock_counter_next;
        dshot_reg          <= dshot_next;
        done_reg           <= done_next;
        period_counter_reg <= period_counter_next;
      end if;
      if i_enable='1' and max_clock then 
        shift_reg <= shift_next;
      end if;
   end if;
  end process;

  -- next-state logic
  max_clock  <= clock_counter_reg = BIT_PERIOD_CLOCK_COUNT-1;
  max_period <= period_counter_reg = unsigned(i_delay)+i_frame'length-1;

  period_counter_next <= (others => '0')      when max_clock and max_period else
                         period_counter_reg+1 when max_clock else
                         period_counter_reg;
  clock_counter_next  <= (others => '0') when max_clock else
                         clock_counter_reg+1;
  done_next <= '1' when max_clock and max_period else '0'; 

  SHIFTREG_LOGIC : for i in 1 to shift_reg'high generate
    shift_next(i) <= i_frame(i) when max_clock and max_period else shift_reg(i-1);
  end generate;
  shift_next(0) <= i_frame(0) when max_clock and max_period else '0';

  dshot_low  <= '1' when clock_counter_reg < BIT_LOW_CLOCK_COUNT  else '0';
  dshot_high <= '1' when clock_counter_reg < BIT_HIGH_CLOCK_COUNT else '0';
  dshot_next <= '0'        when period_counter_reg > i_frame'length-1 else
                dshot_high when shift_reg(shift_reg'high) = '1' else
                dshot_low; 
 
  -- outputs
  o_dshot <= dshot_reg;
  o_done  <= done_reg;
end architecture;
