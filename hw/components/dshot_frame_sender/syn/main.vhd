library ieee;
library edi_interface;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity main is
  generic(
    BIT_PERIOD_CLOCK_COUNT : natural := 1333;
    BITS_IN_RESET          : natural := 21
  );
  port(
    clk     : in  std_logic;
    i_frame :  in std_logic_vector(15 downto 0);
    o_dshot : out std_logic
  );
end entity;

architecture RTL of main is
begin

  COMP : entity work.dshot_frame_sender
  generic map(
    BIT_PERIOD_CLOCK_COUNT => BIT_PERIOD_CLOCK_COUNT,
    BITS_IN_RESET          => BITS_IN_RESET
  )
  port map(
    clk     => clk,
    i_frame => i_frame,
    o_dshot => o_dshot
  );

end architecture;
