library std;
library ieee;
library uvvm_util;
library vunit_lib;
library osvvm;
library edi;
library edi_interface;

context uvvm_util.uvvm_util_context;
context vunit_lib.vunit_context;
context vunit_lib.vc_context;

use std.env.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_pkg.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use vunit_lib.memory_pkg.all;
use vunit_lib.axi_stream_pkg.all;
use vunit_lib.stream_slave_pkg.all;
use vunit_lib.stream_master_pkg.all;
use edi.data_types.all;


entity tb is
  generic(
    runner_cfg : string;
    BIT_PERIOD_CLOCK_COUNT : natural := 1333;
    BITS_IN_RESET          : natural := 21
  );
end entity;


architecture RTL of tb is
  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal clk  : std_logic := '0';
  signal i_frame : std_logic_vector(15 downto 0) := (others => '0');
  signal o_dshot : std_logic := '0';

  -----------------------------------------------------------------------------
  -- Clock related
  -----------------------------------------------------------------------------
  constant CLK_PERIOD : time    := 10 ns;
  signal clk_en       : boolean := true;

  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------
  constant TIMEOUT_INIT : time := CLK_PERIOD*BIT_PERIOD_CLOCK_COUNT*(BITS_IN_RESET+16);
  constant TIMEOUT_BIT  : time := 0.1*CLK_PERIOD*BIT_PERIOD_CLOCK_COUNT;


begin

  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT : entity edi_interface.dshot_frame_sender
  generic map(
    BIT_PERIOD_CLOCK_COUNT => BIT_PERIOD_CLOCK_COUNT,
    BITS_IN_RESET          => BITS_IN_RESET)
  port map(
    clk     => clk,
    i_frame => i_frame,
    o_dshot => o_dshot);

  -----------------------------------------------------------------------------
  -- Clock instantation
  -----------------------------------------------------------------------------
  clock_generator(clk, clk_en, CLK_PERIOD, "TB clock");

  ----------------------------------------------------------------------------
  -- Verification component instantiation
  ----------------------------------------------------------------------------

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    ---------------------------------------------------------------------------
    -- Variables
    ---------------------------------------------------------------------------
    variable v_command   : slv(10 downto 0) := (others => '0');
    variable v_telemetry : slv(0  downto 0) := (others => '0');
    variable v_crc       : slv(3  downto 0) := (others => '0');
    variable v_polynom   : slv(4  downto 0) := "10101";
    variable v_frame     : slv(15 downto 0) := (others => '0');

    ---------------------------------------------------------------------------
    -- Functions
    ---------------------------------------------------------------------------
    function calculate_crc(v_data, v_poly: slv) return slv is
      constant CRC_VECTOR_LENGTH : natural := v_data'length + v_poly'length-1;
      variable v_vector : slv(CRC_VECTOR_LENGTH-1 downto 0) := (others => '0');
    begin
      -- put data in the CRC calculation vector
      v_vector(v_vector'high downto v_vector'high-v_data'length+1) := v_data;

      -- CRC algorithm
      for i in v_vector'high downto v_poly'high loop
        -- skip to next iteration as per algorithm
        if v_vector(i) = '0' then
          next;
        end if;

        -- xor with polynom starting from the highest '1' bit
        v_vector(i downto i-v_poly'length+1)
          := v_vector(i downto i-v_poly'length+1) xor v_poly;
      end loop;

      -- uncomment for function debugging
      --info("poly:"   & to_string(v_poly) 
      --  & "; data: " & to_string(v_data)
      --  & "; crc: "  & to_string(v_vector(CRC_LENGTH-1 downto 0)));
      return v_vector(v_poly'length-1-1 downto 0);
    end function;

    ---------------------------------------------------------------------------
    -- Procedures
    ---------------------------------------------------------------------------
    procedure dut_check_data(v_frame : in slv) is
    begin
      -- wait for transaction start
      wait until rising_edge(o_dshot) for TIMEOUT_INIT;
      check(o_dshot='1', "Timeout (Waited for " & time'image(TIMEOUT_INIT) & ")");

      -- SENDING DATA
      for i in v_frame'low to v_frame'high loop
        --wait until o_dshot='1' for TIMEOUT_BIT;
        --check(o_dshot='1', "Timeout, each frame bit must start with '1' " 
        --  & "(Waited for " & time'image(TIMEOUT_BIT) & ")");


        -- wait for middle
        wait for CLK_PERIOD*BIT_PERIOD_CLOCK_COUNT/2;

        -- check if value is as expected
        check(o_dshot=v_frame(i), "At frame's (" & to_hstring(v_frame) & ") "
          & integer'image(i) & " bit "
          & "EXPECTING: " & std_logic'image(v_frame(i))
          & "GOT: " & std_logic'image(o_dshot));

        -- wait for end
        wait for CLK_PERIOD*BIT_PERIOD_CLOCK_COUNT/2;
      end loop;
    end procedure;

    procedure dut_check_reset is
    begin
      -- EXPECTING RESET
      for i in 0 to BITS_IN_RESET-1 loop
        -- wait for middle
        wait for CLK_PERIOD*BIT_PERIOD_CLOCK_COUNT/2;

        -- check if output is zero as expected
        check(o_dshot='0', "Expecting zero at reset, but GOT: "
          & std_logic'image(o_dshot));

        -- wait for end
        wait for CLK_PERIOD*BIT_PERIOD_CLOCK_COUNT/2;
      end loop;
    end procedure;


  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      if run("full_coverage") then
        v_frame := (others=>'0');
        i_frame <= v_frame;

        for i in 0 to 2**11-1 loop
          -- check data
          dut_check_data(v_frame);

          -- prepare/set frame
          v_command := slv(to_unsigned(i,11));
          v_crc     := calculate_crc(v_command & v_telemetry, v_polynom);
          v_frame   := v_crc & v_telemetry & v_command;
          i_frame   <= v_frame;

          -- check reset
          -- dut_check_reset;
        end loop;
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;

end architecture;
