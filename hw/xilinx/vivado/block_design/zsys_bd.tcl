catch {TE::UTILS::te_msg TE_BD-0 INFO "This block design tcl-file was generate with Trenz Electronic GmbH Board Part:trenz.biz:te0727_10_1c:part0:1.0, FPGA: xc7z010clg225-1 at 2020-07-07T12:27:54."}
catch {TE::UTILS::te_msg TE_BD-1 INFO "This block design tcl-file was modified by TE-Scripts. Modifications are labelled with comment tag  # #TE_MOD# on the Block-Design tcl-file."}

if { ![info exist TE::VERSION_CONTROL] } {
    set TE::VERSION_CONTROL true
}
################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.2
set current_vivado_version [version -short ]
if { [string first $scripts_vivado_version $current_vivado_version] == -1 &&  $TE::VERSION_CONTROL } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado < $scripts_vivado_version> and is being run in < $current_vivado_version> of Vivado. Please run the script in Vivado < $scripts_vivado_version> then open the design in Vivado < $current_vivado_version>. Upgrade the design by running "Tools => Report => Report IP Status...", then run write_bd_tcl to create an updated script."}
 return 1
}

################################################################
# This is a generated script based on design: zsys
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   common::send_msg_id "BD_TCL-1002" "WARNING" "This script was generated using Vivado <$scripts_vivado_version> without IP versions in the create_bd_cell commands, but is now being run in <$current_vivado_version> of Vivado. There may have been major IP version changes between Vivado <$scripts_vivado_version> and <$current_vivado_version>, which could impact the parameter settings of the IPs."

}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source zsys_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z010clg225-1
   set_property BOARD_PART trenz.biz:te0727_10_1c:part0:1.0 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name zsys

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:xlconstant:*\
xilinx.com:ip:axi_gpio:*\
xilinx.com:ip:c_counter_binary:*\
xilinx.com:ip:processing_system7:*\
xilinx.com:ip:proc_sys_reset:*\
xilinx.com:ip:vio:*\
xilinx.com:ip:xlconcat:*\
xilinx.com:ip:xlslice:*\
xilinx.com:ip:axi_vdma:*\
trenz.biz:user:axis_csi2_rx:*\
xilinx.com:ip:axis_data_fifo:*\
trenz.biz:user:axis_raw_demosaic:*\
trenz.biz:user:axis_raw_unpack:*\
trenz.biz:user:axis_video_crop:*\
trenz.biz:user:Video_IO_2_HDMI_TMDS:*\
xilinx.com:ip:clk_wiz:*\
xilinx.com:ip:v_axi4s_vid_out:*\
xilinx.com:ip:v_tc:*\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: video_out
proc create_hier_cell_video_out { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_video_out() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M00_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE


  # Create pins
  create_bd_pin -dir I -type rst axi_int_aresetn
  create_bd_pin -dir I -type rst axi_resetn
  create_bd_pin -dir O hdmi_clk_n
  create_bd_pin -dir O hdmi_clk_p
  create_bd_pin -dir O -from 2 -to 0 hdmi_data_n
  create_bd_pin -dir O -from 2 -to 0 hdmi_data_p
  create_bd_pin -dir O -type intr mm2s_introut
  create_bd_pin -dir I -type clk s_axi_lite_aclk
  create_bd_pin -dir I -type rst vid_io_out_reset

  # Create instance: Video_IO_2_HDMI_TMDS_0, and set properties
  set Video_IO_2_HDMI_TMDS_0 [ create_bd_cell -type ip -vlnv trenz.biz:user:Video_IO_2_HDMI_TMDS Video_IO_2_HDMI_TMDS_0 ]
  set_property -dict [ list \
   CONFIG.C_CLK_SWAP {true} \
   CONFIG.C_D0_SWAP {true} \
   CONFIG.C_D1_SWAP {true} \
   CONFIG.C_INT_CLOCKING {false} \
 ] $Video_IO_2_HDMI_TMDS_0

  # Create instance: axi_interconnect_0, and set properties
  set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect axi_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.NUM_MI {1} \
 ] $axi_interconnect_0

  # Create instance: axi_vdma_0, and set properties
  set axi_vdma_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_vdma axi_vdma_0 ]
  set_property -dict [ list \
   CONFIG.c_include_s2mm {0} \
   CONFIG.c_m_axi_mm2s_data_width {32} \
   CONFIG.c_mm2s_genlock_mode {0} \
   CONFIG.c_mm2s_linebuffer_depth {1024} \
   CONFIG.c_mm2s_max_burst_length {16} \
   CONFIG.c_num_fstores {1} \
   CONFIG.c_s2mm_genlock_mode {0} \
 ] $axi_vdma_0

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKOUT1_JITTER {337.616} \
   CONFIG.CLKOUT1_PHASE_ERROR {322.999} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {74.250} \
   CONFIG.CLKOUT2_JITTER {258.703} \
   CONFIG.CLKOUT2_PHASE_ERROR {322.999} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {371.250} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {37.125} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {10.000} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {2} \
   CONFIG.MMCM_DIVCLK_DIVIDE {5} \
   CONFIG.NUM_OUT_CLKS {2} \
   CONFIG.USE_LOCKED {true} \
   CONFIG.USE_RESET {false} \
 ] $clk_wiz_0

  # Create instance: v_axi4s_vid_out_0, and set properties
  set v_axi4s_vid_out_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:v_axi4s_vid_out v_axi4s_vid_out_0 ]
  set_property -dict [ list \
   CONFIG.C_HAS_ASYNC_CLK {1} \
 ] $v_axi4s_vid_out_0

  # Create instance: v_tc_0, and set properties
  set v_tc_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:v_tc v_tc_0 ]
  set_property -dict [ list \
   CONFIG.HAS_AXI4_LITE {false} \
   CONFIG.enable_detection {false} \
 ] $v_tc_0

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.IN0_WIDTH {8} \
   CONFIG.IN1_WIDTH {8} \
   CONFIG.IN2_WIDTH {8} \
   CONFIG.NUM_PORTS {3} \
 ] $xlconcat_0

  # Create instance: xlconstant_high, and set properties
  set xlconstant_high [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant xlconstant_high ]

  # Create instance: xlconstant_zero, and set properties
  set xlconstant_zero [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant xlconstant_zero ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_zero

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {7} \
   CONFIG.DOUT_WIDTH {8} \
 ] $xlslice_0

  # Create instance: xlslice_1, and set properties
  set xlslice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {15} \
   CONFIG.DIN_TO {8} \
   CONFIG.DOUT_WIDTH {8} \
 ] $xlslice_1

  # Create instance: xlslice_2, and set properties
  set xlslice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {23} \
   CONFIG.DIN_TO {16} \
   CONFIG.DOUT_WIDTH {8} \
 ] $xlslice_2

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins M00_AXI] [get_bd_intf_pins axi_interconnect_0/M00_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S_AXI_LITE] [get_bd_intf_pins axi_vdma_0/S_AXI_LITE]
  connect_bd_intf_net -intf_net axi_vdma_0_M_AXI_MM2S [get_bd_intf_pins axi_interconnect_0/S00_AXI] [get_bd_intf_pins axi_vdma_0/M_AXI_MM2S]
  connect_bd_intf_net -intf_net v_axi4s_vid_out_0_vid_io_out [get_bd_intf_pins Video_IO_2_HDMI_TMDS_0/vid_io_in] [get_bd_intf_pins v_axi4s_vid_out_0/vid_io_out]
  connect_bd_intf_net -intf_net v_tc_0_vtiming_out [get_bd_intf_pins v_axi4s_vid_out_0/vtiming_in] [get_bd_intf_pins v_tc_0/vtiming_out]

  # Create port connections
  connect_bd_net -net ARESETN_1 [get_bd_pins axi_int_aresetn] [get_bd_pins axi_interconnect_0/ARESETN]
  connect_bd_net -net Video_IO_2_HDMI_TMDS_0_hdmi_clk_n [get_bd_pins hdmi_clk_n] [get_bd_pins Video_IO_2_HDMI_TMDS_0/hdmi_clk_n]
  connect_bd_net -net Video_IO_2_HDMI_TMDS_0_hdmi_clk_p [get_bd_pins hdmi_clk_p] [get_bd_pins Video_IO_2_HDMI_TMDS_0/hdmi_clk_p]
  connect_bd_net -net Video_IO_2_HDMI_TMDS_0_hdmi_data_n [get_bd_pins hdmi_data_n] [get_bd_pins Video_IO_2_HDMI_TMDS_0/hdmi_data_n]
  connect_bd_net -net Video_IO_2_HDMI_TMDS_0_hdmi_data_p [get_bd_pins hdmi_data_p] [get_bd_pins Video_IO_2_HDMI_TMDS_0/hdmi_data_p]
  connect_bd_net -net axi_resetn_1 [get_bd_pins axi_resetn] [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins axi_interconnect_0/S00_ARESETN] [get_bd_pins axi_vdma_0/axi_resetn] [get_bd_pins v_axi4s_vid_out_0/aresetn] [get_bd_pins v_tc_0/resetn]
  connect_bd_net -net axi_vdma_0_m_axis_mm2s_tdata [get_bd_pins axi_vdma_0/m_axis_mm2s_tdata] [get_bd_pins xlslice_0/Din] [get_bd_pins xlslice_1/Din] [get_bd_pins xlslice_2/Din]
  connect_bd_net -net axi_vdma_0_m_axis_mm2s_tlast [get_bd_pins axi_vdma_0/m_axis_mm2s_tlast] [get_bd_pins v_axi4s_vid_out_0/s_axis_video_tlast]
  connect_bd_net -net axi_vdma_0_m_axis_mm2s_tuser [get_bd_pins axi_vdma_0/m_axis_mm2s_tuser] [get_bd_pins v_axi4s_vid_out_0/s_axis_video_tuser]
  connect_bd_net -net axi_vdma_0_m_axis_mm2s_tvalid [get_bd_pins axi_vdma_0/m_axis_mm2s_tvalid] [get_bd_pins v_axi4s_vid_out_0/s_axis_video_tvalid]
  connect_bd_net -net axi_vdma_0_mm2s_introut [get_bd_pins mm2s_introut] [get_bd_pins axi_vdma_0/mm2s_introut]
  connect_bd_net -net clk_wiz_0_clk_out2 [get_bd_pins Video_IO_2_HDMI_TMDS_0/video_clk5x_in] [get_bd_pins clk_wiz_0/clk_out2]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins Video_IO_2_HDMI_TMDS_0/lock_in] [get_bd_pins clk_wiz_0/locked]
  connect_bd_net -net s_axi_lite_aclk_1 [get_bd_pins s_axi_lite_aclk] [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins axi_interconnect_0/S00_ACLK] [get_bd_pins axi_vdma_0/m_axi_mm2s_aclk] [get_bd_pins axi_vdma_0/m_axis_mm2s_aclk] [get_bd_pins axi_vdma_0/s_axi_lite_aclk] [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins v_axi4s_vid_out_0/aclk]
  connect_bd_net -net v_axi4s_vid_out_0_s_axis_video_tready [get_bd_pins axi_vdma_0/m_axis_mm2s_tready] [get_bd_pins v_axi4s_vid_out_0/s_axis_video_tready]
  connect_bd_net -net v_axi4s_vid_out_0_vtg_ce [get_bd_pins v_axi4s_vid_out_0/vtg_ce] [get_bd_pins v_tc_0/gen_clken]
  connect_bd_net -net vid_io_out_clk_1 [get_bd_pins Video_IO_2_HDMI_TMDS_0/video_clk_in] [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins v_axi4s_vid_out_0/vid_io_out_clk] [get_bd_pins v_tc_0/clk]
  connect_bd_net -net vid_io_out_reset_1 [get_bd_pins vid_io_out_reset] [get_bd_pins v_axi4s_vid_out_0/vid_io_out_reset]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins v_axi4s_vid_out_0/s_axis_video_tdata] [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlconstant_high_dout [get_bd_pins v_axi4s_vid_out_0/aclken] [get_bd_pins v_axi4s_vid_out_0/vid_io_out_ce] [get_bd_pins v_tc_0/clken] [get_bd_pins xlconstant_high/dout]
  connect_bd_net -net xlconstant_zero_dout [get_bd_pins v_axi4s_vid_out_0/fid] [get_bd_pins xlconstant_zero/dout]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins xlconcat_0/In2] [get_bd_pins xlslice_0/Dout]
  connect_bd_net -net xlslice_1_Dout [get_bd_pins xlconcat_0/In0] [get_bd_pins xlslice_1/Dout]
  connect_bd_net -net xlslice_2_Dout [get_bd_pins xlconcat_0/In1] [get_bd_pins xlslice_2/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: video_in
proc create_hier_cell_video_in { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_video_in() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M00_AXI

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE


  # Create pins
  create_bd_pin -dir I -type clk axi_aclk
  create_bd_pin -dir I -type rst axi_aresetn
  create_bd_pin -dir I -type rst axi_int_aresetn
  create_bd_pin -dir I clk_rxn_0
  create_bd_pin -dir I clk_rxp_0
  create_bd_pin -dir I -from 31 -to 0 control
  create_bd_pin -dir I -from 1 -to 0 csi_d_n
  create_bd_pin -dir I -from 1 -to 0 csi_d_p
  create_bd_pin -dir O dma_int
  create_bd_pin -dir I ext_resetn
  create_bd_pin -dir I -from 1 -to 0 lp_n
  create_bd_pin -dir I -from 1 -to 0 lp_p
  create_bd_pin -dir I processing_clk
  create_bd_pin -dir I ref_clk

  # Create instance: axi_interconnect_0, and set properties
  set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect axi_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.NUM_MI {1} \
 ] $axi_interconnect_0

  # Create instance: axi_vdma_0, and set properties
  set axi_vdma_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_vdma axi_vdma_0 ]
  set_property -dict [ list \
   CONFIG.c_include_mm2s {0} \
   CONFIG.c_include_s2mm_dre {1} \
   CONFIG.c_m_axi_s2mm_data_width {64} \
   CONFIG.c_mm2s_genlock_mode {0} \
   CONFIG.c_num_fstores {1} \
   CONFIG.c_s2mm_genlock_mode {0} \
   CONFIG.c_s2mm_linebuffer_depth {4096} \
   CONFIG.c_s2mm_max_burst_length {32} \
 ] $axi_vdma_0

  # Create instance: axis_csi2_rx_0, and set properties
  set axis_csi2_rx_0 [ create_bd_cell -type ip -vlnv trenz.biz:user:axis_csi2_rx axis_csi2_rx_0 ]
  set_property -dict [ list \
   CONFIG.C_ADD_IDELAYCTRL {true} \
   CONFIG.C_NUM_LP_LANES {2} \
 ] $axis_csi2_rx_0

  # Create instance: axis_data_fifo_0, and set properties
  set axis_data_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo axis_data_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16384} \
   CONFIG.HAS_TLAST {1} \
   CONFIG.IS_ACLK_ASYNC {1} \
   CONFIG.TDATA_NUM_BYTES {2} \
   CONFIG.TUSER_WIDTH {1} \
 ] $axis_data_fifo_0

  # Create instance: axis_data_fifo_1, and set properties
  set axis_data_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo axis_data_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {1024} \
 ] $axis_data_fifo_1

  # Create instance: axis_data_fifo_2, and set properties
  set axis_data_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo axis_data_fifo_2 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {4096} \
   CONFIG.IS_ACLK_ASYNC {1} \
 ] $axis_data_fifo_2

  # Create instance: axis_raw_demosaic_0, and set properties
  set axis_raw_demosaic_0 [ create_bd_cell -type ip -vlnv trenz.biz:user:axis_raw_demosaic axis_raw_demosaic_0 ]
  set_property -dict [ list \
   CONFIG.C_COLOR_POS {2} \
   CONFIG.C_MODE {2} \
 ] $axis_raw_demosaic_0

  # Create instance: axis_raw_unpack_0, and set properties
  set axis_raw_unpack_0 [ create_bd_cell -type ip -vlnv trenz.biz:user:axis_raw_unpack axis_raw_unpack_0 ]
  set_property -dict [ list \
   CONFIG.C_IMP_TYPE {1} \
   CONFIG.C_OUT_TYPE {1} \
 ] $axis_raw_unpack_0

  # Create instance: axis_video_crop_0, and set properties
  set axis_video_crop_0 [ create_bd_cell -type ip -vlnv trenz.biz:user:axis_video_crop axis_video_crop_0 ]
  set_property -dict [ list \
   CONFIG.C_MODE {2} \
   CONFIG.C_X_OFF {2} \
   CONFIG.C_Y_OFF {2} \
 ] $axis_video_crop_0

  # Create instance: proc_sys_reset_0, and set properties
  set proc_sys_reset_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset proc_sys_reset_0 ]

  # Create instance: proc_sys_reset_1, and set properties
  set proc_sys_reset_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset proc_sys_reset_1 ]

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_0 ]

  # Create instance: xlslice_1, and set properties
  set xlslice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_1

  # Create instance: xlslice_2, and set properties
  set xlslice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_2

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins M00_AXI] [get_bd_intf_pins axi_interconnect_0/M00_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S_AXI_LITE] [get_bd_intf_pins axi_vdma_0/S_AXI_LITE]
  connect_bd_intf_net -intf_net axi_vdma_0_M_AXI_S2MM [get_bd_intf_pins axi_interconnect_0/S00_AXI] [get_bd_intf_pins axi_vdma_0/M_AXI_S2MM]
  connect_bd_intf_net -intf_net axis_csi2_rx_0_m_axis [get_bd_intf_pins axis_csi2_rx_0/m_axis] [get_bd_intf_pins axis_data_fifo_0/S_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_0_M_AXIS [get_bd_intf_pins axis_data_fifo_0/M_AXIS] [get_bd_intf_pins axis_raw_unpack_0/S_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_1_M_AXIS [get_bd_intf_pins axis_data_fifo_1/M_AXIS] [get_bd_intf_pins axis_raw_demosaic_0/S_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_2_M_AXIS [get_bd_intf_pins axi_vdma_0/S_AXIS_S2MM] [get_bd_intf_pins axis_data_fifo_2/M_AXIS]
  connect_bd_intf_net -intf_net axis_raw_demosaic_0_M_AXIS [get_bd_intf_pins axis_raw_demosaic_0/M_AXIS] [get_bd_intf_pins axis_video_crop_0/s_axis]
  connect_bd_intf_net -intf_net axis_raw_unpack_0_M_AXIS [get_bd_intf_pins axis_data_fifo_1/S_AXIS] [get_bd_intf_pins axis_raw_unpack_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_video_crop_0_m_axis [get_bd_intf_pins axis_data_fifo_2/S_AXIS] [get_bd_intf_pins axis_video_crop_0/m_axis]

  # Create port connections
  connect_bd_net -net ARESETN_1 [get_bd_pins axi_int_aresetn] [get_bd_pins axi_interconnect_0/ARESETN]
  connect_bd_net -net M00_ARESETN_1 [get_bd_pins axi_aresetn] [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins axi_interconnect_0/S00_ARESETN] [get_bd_pins axi_vdma_0/axi_resetn]
  connect_bd_net -net S00_ACLK_1 [get_bd_pins axi_aclk] [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins axi_interconnect_0/S00_ACLK] [get_bd_pins axi_vdma_0/m_axi_s2mm_aclk] [get_bd_pins axi_vdma_0/s_axi_lite_aclk] [get_bd_pins axi_vdma_0/s_axis_s2mm_aclk] [get_bd_pins axis_data_fifo_2/m_axis_aclk]
  connect_bd_net -net axi_vdma_0_s2mm_introut [get_bd_pins dma_int] [get_bd_pins axi_vdma_0/s2mm_introut]
  connect_bd_net -net axis_csi2_rx_0_axis_aclk [get_bd_pins axis_csi2_rx_0/axis_aclk] [get_bd_pins axis_data_fifo_0/s_axis_aclk] [get_bd_pins proc_sys_reset_0/slowest_sync_clk]
  connect_bd_net -net clk_rxn_0_1 [get_bd_pins clk_rxn_0] [get_bd_pins axis_csi2_rx_0/clk_rxn]
  connect_bd_net -net clk_rxp_0_1 [get_bd_pins clk_rxp_0] [get_bd_pins axis_csi2_rx_0/clk_rxp]
  connect_bd_net -net control_1 [get_bd_pins control] [get_bd_pins xlslice_0/Din] [get_bd_pins xlslice_1/Din] [get_bd_pins xlslice_2/Din]
  connect_bd_net -net csi_d_n_1 [get_bd_pins csi_d_n] [get_bd_pins axis_csi2_rx_0/data_rxn]
  connect_bd_net -net csi_d_p_1 [get_bd_pins csi_d_p] [get_bd_pins axis_csi2_rx_0/data_rxp]
  connect_bd_net -net ext_resetn_1 [get_bd_pins ext_resetn] [get_bd_pins proc_sys_reset_0/ext_reset_in] [get_bd_pins proc_sys_reset_1/ext_reset_in]
  connect_bd_net -net lp_n_1 [get_bd_pins lp_n] [get_bd_pins axis_csi2_rx_0/data_lp_n]
  connect_bd_net -net lp_p_1 [get_bd_pins lp_p] [get_bd_pins axis_csi2_rx_0/data_lp_p]
  connect_bd_net -net proc_sys_reset_0_peripheral_aresetn [get_bd_pins axis_data_fifo_0/s_axis_aresetn] [get_bd_pins proc_sys_reset_0/peripheral_aresetn]
  connect_bd_net -net proc_sys_reset_1_peripheral_aresetn [get_bd_pins axis_data_fifo_1/s_axis_aresetn] [get_bd_pins axis_data_fifo_2/s_axis_aresetn] [get_bd_pins axis_raw_demosaic_0/axis_aresetn] [get_bd_pins axis_raw_unpack_0/axis_aresetn] [get_bd_pins proc_sys_reset_1/peripheral_aresetn]
  connect_bd_net -net processing_clk_1 [get_bd_pins processing_clk] [get_bd_pins axis_data_fifo_0/m_axis_aclk] [get_bd_pins axis_data_fifo_1/s_axis_aclk] [get_bd_pins axis_data_fifo_2/s_axis_aclk] [get_bd_pins axis_raw_demosaic_0/axis_aclk] [get_bd_pins axis_raw_unpack_0/axis_aclk] [get_bd_pins axis_video_crop_0/axis_aclk] [get_bd_pins proc_sys_reset_1/slowest_sync_clk]
  connect_bd_net -net ref_clk_1 [get_bd_pins ref_clk] [get_bd_pins axis_csi2_rx_0/in_delay_clk]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins axis_csi2_rx_0/enable_in] [get_bd_pins xlslice_0/Dout]
  connect_bd_net -net xlslice_1_Dout [get_bd_pins axis_raw_demosaic_0/colors_mode] [get_bd_pins xlslice_1/Dout]
  connect_bd_net -net xlslice_2_Dout [get_bd_pins axis_raw_demosaic_0/system_mode] [get_bd_pins axis_video_crop_0/disable_in] [get_bd_pins xlslice_2/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
# #TE_MOD#   set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]

# #TE_MOD#   set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]

  set GPIO [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 GPIO ]

  set IIC_A [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 IIC_A ]


  # Create ports
  set CEC_A [ create_bd_port -dir O -from 0 -to 0 CEC_A ]
  set CLP_D_N [ create_bd_port -dir I -from 1 -to 0 CLP_D_N ]
  set CLP_D_P [ create_bd_port -dir I -from 1 -to 0 CLP_D_P ]
  set CSI_C_N [ create_bd_port -dir I CSI_C_N ]
  set CSI_C_P [ create_bd_port -dir I CSI_C_P ]
  set CSI_D_N [ create_bd_port -dir I -from 1 -to 0 CSI_D_N ]
  set CSI_D_P [ create_bd_port -dir I -from 1 -to 0 CSI_D_P ]
  set CT_HPD [ create_bd_port -dir O -from 0 -to 0 CT_HPD ]
  set GLED [ create_bd_port -dir O -from 0 -to 0 GLED ]
  set HDMI_TXC_N [ create_bd_port -dir O HDMI_TXC_N ]
  set HDMI_TXC_P [ create_bd_port -dir O HDMI_TXC_P ]
  set HDMI_TX_N [ create_bd_port -dir O -from 2 -to 0 HDMI_TX_N ]
  set HDMI_TX_P [ create_bd_port -dir O -from 2 -to 0 HDMI_TX_P ]
  set HPD_A [ create_bd_port -dir I HPD_A ]

  # Create instance: LOW, and set properties
  set LOW [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant LOW ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $LOW

  # Create instance: axi_gpio_0, and set properties
  set axi_gpio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio axi_gpio_0 ]
  set_property -dict [ list \
   CONFIG.C_DOUT_DEFAULT {0x00000000} \
   CONFIG.C_GPIO2_WIDTH {32} \
   CONFIG.C_GPIO_WIDTH {28} \
   CONFIG.C_IS_DUAL {0} \
   CONFIG.C_TRI_DEFAULT {0xFFFFFFFF} \
 ] $axi_gpio_0

  # Create instance: axi_gpio_1, and set properties
  set axi_gpio_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio axi_gpio_1 ]
  set_property -dict [ list \
   CONFIG.C_ALL_OUTPUTS {1} \
 ] $axi_gpio_1

  # Create instance: c_counter_binary_0, and set properties
  set c_counter_binary_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_counter_binary c_counter_binary_0 ]
  set_property -dict [ list \
   CONFIG.Output_Width {24} \
 ] $c_counter_binary_0

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7 processing_system7_0 ]
# #TE_MOD#_Add next line#
  apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1"}  [get_bd_cells processing_system7_0]
# #TE_MOD#_Add next line#
  set tcl_pr_ext [];if { [catch {set tcl_pr_ext [glob -join -dir ${TE::BOARDDEF_PATH}/preset_extension/ *_preset.tcl]}] } {};foreach preset_ext $tcl_pr_ext { source  $preset_ext};
# #TE_MOD#   set_property -dict [ list \
# #TE_MOD#  ] $processing_system7_0

# #TE_MOD#    CONFIG.PCW_WDT_WDT_IO {EMIO} \
# #TE_MOD#    CONFIG.PCW_WDT_PERIPHERAL_FREQMHZ {133.333333} \
# #TE_MOD#    CONFIG.PCW_WDT_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_USE_S_AXI_HP1 {1} \
# #TE_MOD#    CONFIG.PCW_USE_S_AXI_HP0 {1} \
# #TE_MOD#    CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
# #TE_MOD#    CONFIG.PCW_USB_RESET_SELECT {Share reset pin} \
# #TE_MOD#    CONFIG.PCW_USB_RESET_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_USB1_RESET_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
# #TE_MOD#    CONFIG.PCW_USB0_RESET_IO {MIO 7} \
# #TE_MOD#    CONFIG.PCW_USB0_RESET_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_USB0_PERIPHERAL_FREQMHZ {60} \
# #TE_MOD#    CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_T_RP {7} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_T_RCD {7} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_T_RC {48.91} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_T_RAS_MIN {35.0} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_T_FAW {40.0} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_SPEED_BIN {DDR3_1066F} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_ROW_ADDR_COUNT {15} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J256M16 RE-125} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_MEMORY_TYPE {DDR 3 (Low Voltage)} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_DRAM_WIDTH {16 Bits} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_DEVICE_CAPACITY {4096 MBits} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_CWL {6} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_COL_ADDR_COUNT {10} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_CL {7} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_BL {8} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_DDR_BANK_ADDR_COUNT {3} \
# #TE_MOD#    CONFIG.PCW_UIPARAM_ACT_DDR_FREQ_MHZ {533.333374} \
# #TE_MOD#    CONFIG.PCW_UART_PERIPHERAL_VALID {1} \
# #TE_MOD#    CONFIG.PCW_UART_PERIPHERAL_FREQMHZ {100} \
# #TE_MOD#    CONFIG.PCW_UART_PERIPHERAL_DIVISOR0 {16} \
# #TE_MOD#    CONFIG.PCW_UART1_UART1_IO {MIO 8 .. 9} \
# #TE_MOD#    CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_UART1_GRP_FULL_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_TTC_PERIPHERAL_FREQMHZ {50} \
# #TE_MOD#    CONFIG.PCW_TTC1_TTC1_IO {EMIO} \
# #TE_MOD#    CONFIG.PCW_TTC1_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_TTC1_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
# #TE_MOD#    CONFIG.PCW_TTC1_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
# #TE_MOD#    CONFIG.PCW_TTC1_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
# #TE_MOD#    CONFIG.PCW_TTC0_TTC0_IO {EMIO} \
# #TE_MOD#    CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_TTC0_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
# #TE_MOD#    CONFIG.PCW_TTC0_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
# #TE_MOD#    CONFIG.PCW_TTC0_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
# #TE_MOD#    CONFIG.PCW_TPIU_PERIPHERAL_DIVISOR0 {1} \
# #TE_MOD#    CONFIG.PCW_SPI_PERIPHERAL_DIVISOR0 {1} \
# #TE_MOD#    CONFIG.PCW_SMC_PERIPHERAL_DIVISOR0 {1} \
# #TE_MOD#    CONFIG.PCW_SINGLE_QSPI_DATA_MODE {x4} \
# #TE_MOD#    CONFIG.PCW_SDIO_PERIPHERAL_VALID {1} \
# #TE_MOD#    CONFIG.PCW_SDIO_PERIPHERAL_FREQMHZ {100} \
# #TE_MOD#    CONFIG.PCW_SDIO_PERIPHERAL_DIVISOR0 {16} \
# #TE_MOD#    CONFIG.PCW_SD1_SD1_IO {MIO 10 .. 15} \
# #TE_MOD#    CONFIG.PCW_SD1_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_SD1_GRP_WP_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_SD1_GRP_POW_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_SD1_GRP_CD_IO {MIO 0} \
# #TE_MOD#    CONFIG.PCW_SD1_GRP_CD_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} \
# #TE_MOD#    CONFIG.PCW_QSPI_PERIPHERAL_FREQMHZ {200} \
# #TE_MOD#    CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_QSPI_PERIPHERAL_DIVISOR0 {8} \
# #TE_MOD#    CONFIG.PCW_QSPI_GRP_SS1_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_QSPI_GRP_SINGLE_SS_IO {MIO 1 .. 6} \
# #TE_MOD#    CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_QSPI_GRP_IO1_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_PRESET_BANK0_VOLTAGE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_PCAP_PERIPHERAL_DIVISOR0 {8} \
# #TE_MOD#    CONFIG.PCW_NOR_PERIPHERAL_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_NOR_GRP_SRAM_INT_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_NOR_GRP_SRAM_CS1_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_NOR_GRP_SRAM_CS0_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_NOR_GRP_CS1_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_NOR_GRP_CS0_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_NOR_GRP_A25_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_NAND_PERIPHERAL_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_NAND_GRP_D8_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_MIO_TREE_SIGNALS {cd#qspi0_ss_b#qspi0_io[0]#qspi0_io[1]#qspi0_io[2]#qspi0_io[3]/HOLD_B#qspi0_sclk#reset#tx#rx#data[0]#cmd#clk#data[1]#data[2]#data[3]#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#data[4]#dir#stp#nxt#data[0]#data[1]#data[2]#data[3]#clk#data[5]#data[6]#data[7]#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#scl#sda#Unbonded#Unbonded#gpio[52]#gpio[53]} \
# #TE_MOD#    CONFIG.PCW_MIO_TREE_PERIPHERALS {SD 1#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#USB Reset#UART 1#UART 1#SD 1#SD 1#SD 1#SD 1#SD 1#SD 1#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#Unbonded#I2C 1#I2C 1#Unbonded#Unbonded#GPIO#GPIO} \
# #TE_MOD#    CONFIG.PCW_MIO_9_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_9_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_9_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_9_DIRECTION {in} \
# #TE_MOD#    CONFIG.PCW_MIO_8_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_8_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_8_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_8_DIRECTION {out} \
# #TE_MOD#    CONFIG.PCW_MIO_7_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_7_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_7_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_7_DIRECTION {out} \
# #TE_MOD#    CONFIG.PCW_MIO_6_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_6_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_6_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_6_DIRECTION {out} \
# #TE_MOD#    CONFIG.PCW_MIO_5_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_5_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_5_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_5_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_53_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_53_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_53_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_53_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_52_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_52_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_52_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_52_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_4_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_4_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_4_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_4_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_49_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_49_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_49_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_49_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_48_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_48_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_48_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_48_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_3_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_3_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_3_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_3_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_39_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_39_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_39_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_39_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_38_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_38_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_38_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_38_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_37_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_37_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_37_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_37_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_36_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_36_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_36_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_36_DIRECTION {in} \
# #TE_MOD#    CONFIG.PCW_MIO_35_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_35_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_35_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_35_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_34_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_34_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_34_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_34_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_33_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_33_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_33_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_33_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_32_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_32_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_32_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_32_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_31_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_31_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_31_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_31_DIRECTION {in} \
# #TE_MOD#    CONFIG.PCW_MIO_30_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_30_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_30_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_30_DIRECTION {out} \
# #TE_MOD#    CONFIG.PCW_MIO_2_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_2_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_2_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_2_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_29_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_29_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_29_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_29_DIRECTION {in} \
# #TE_MOD#    CONFIG.PCW_MIO_28_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_28_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_28_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_28_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_1_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_1_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_1_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_1_DIRECTION {out} \
# #TE_MOD#    CONFIG.PCW_MIO_15_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_15_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_15_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_15_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_14_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_14_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_14_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_14_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_13_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_13_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_13_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_13_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_12_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_12_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_12_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_12_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_11_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_11_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_11_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_11_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_10_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_10_PULLUP {disabled} \
# #TE_MOD#    CONFIG.PCW_MIO_10_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_10_DIRECTION {inout} \
# #TE_MOD#    CONFIG.PCW_MIO_0_SLEW {slow} \
# #TE_MOD#    CONFIG.PCW_MIO_0_PULLUP {enabled} \
# #TE_MOD#    CONFIG.PCW_MIO_0_IOTYPE {LVCMOS 3.3V} \
# #TE_MOD#    CONFIG.PCW_MIO_0_DIRECTION {in} \
# #TE_MOD#    CONFIG.PCW_IRQ_F2P_INTR {1} \
# #TE_MOD#    CONFIG.PCW_IO_IO_PLL_FREQMHZ {1600.000} \
# #TE_MOD#    CONFIG.PCW_IOPLL_CTRL_FBDIV {48} \
# #TE_MOD#    CONFIG.PCW_I2C_RESET_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_I2C_PERIPHERAL_FREQMHZ {111.111115} \
# #TE_MOD#    CONFIG.PCW_I2C1_RESET_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_I2C1_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_I2C1_I2C1_IO {MIO 48 .. 49} \
# #TE_MOD#    CONFIG.PCW_I2C1_GRP_INT_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_I2C0_RESET_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_I2C0_I2C0_IO {EMIO} \
# #TE_MOD#    CONFIG.PCW_I2C0_GRP_INT_IO {EMIO} \
# #TE_MOD#    CONFIG.PCW_I2C0_GRP_INT_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_GPIO_MIO_GPIO_IO {MIO} \
# #TE_MOD#    CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_FPGA_FCLK3_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_FPGA_FCLK2_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_FPGA_FCLK1_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_FPGA_FCLK0_ENABLE {1} \
# #TE_MOD#    CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {200} \
# #TE_MOD#    CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100} \
# #TE_MOD#    CONFIG.PCW_FCLK_CLK1_BUF {TRUE} \
# #TE_MOD#    CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR1 {1} \
# #TE_MOD#    CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR0 {1} \
# #TE_MOD#    CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR1 {1} \
# #TE_MOD#    CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR0 {1} \
# #TE_MOD#    CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR1 {2} \
# #TE_MOD#    CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR0 {4} \
# #TE_MOD#    CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR1 {4} \
# #TE_MOD#    CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR0 {4} \
# #TE_MOD#    CONFIG.PCW_EN_WDT {1} \
# #TE_MOD#    CONFIG.PCW_EN_USB0 {1} \
# #TE_MOD#    CONFIG.PCW_EN_UART1 {1} \
# #TE_MOD#    CONFIG.PCW_EN_TTC1 {1} \
# #TE_MOD#    CONFIG.PCW_EN_TTC0 {1} \
# #TE_MOD#    CONFIG.PCW_EN_SDIO1 {1} \
# #TE_MOD#    CONFIG.PCW_EN_QSPI {1} \
# #TE_MOD#    CONFIG.PCW_EN_I2C1 {1} \
# #TE_MOD#    CONFIG.PCW_EN_I2C0 {1} \
# #TE_MOD#    CONFIG.PCW_EN_GPIO {1} \
# #TE_MOD#    CONFIG.PCW_EN_EMIO_WP_SDIO1 {0} \
# #TE_MOD#    CONFIG.PCW_EN_EMIO_WDT {1} \
# #TE_MOD#    CONFIG.PCW_EN_EMIO_TTC1 {1} \
# #TE_MOD#    CONFIG.PCW_EN_EMIO_TTC0 {1} \
# #TE_MOD#    CONFIG.PCW_EN_EMIO_SDIO1 {0} \
# #TE_MOD#    CONFIG.PCW_EN_EMIO_I2C0 {1} \
# #TE_MOD#    CONFIG.PCW_EN_EMIO_CD_SDIO1 {0} \
# #TE_MOD#    CONFIG.PCW_EN_CLK1_PORT {1} \
# #TE_MOD#    CONFIG.PCW_ENET_RESET_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_ENET1_RESET_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR1 {1} \
# #TE_MOD#    CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR0 {1} \
# #TE_MOD#    CONFIG.PCW_ENET0_RESET_ENABLE {0} \
# #TE_MOD#    CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR1 {1} \
# #TE_MOD#    CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR0 {1} \
# #TE_MOD#    CONFIG.PCW_DDR_RAM_HIGHADDR {0x1FFFFFFF} \
# #TE_MOD#    CONFIG.PCW_DDR_PERIPHERAL_DIVISOR0 {2} \
# #TE_MOD#    CONFIG.PCW_DDR_DDR_PLL_FREQMHZ {1066.667} \
# #TE_MOD#    CONFIG.PCW_DDRPLL_CTRL_FBDIV {32} \
# #TE_MOD#    CONFIG.PCW_DCI_PERIPHERAL_DIVISOR1 {7} \
# #TE_MOD#    CONFIG.PCW_DCI_PERIPHERAL_DIVISOR0 {15} \
# #TE_MOD#    CONFIG.PCW_CRYSTAL_PERIPHERAL_FREQMHZ {33.333333} \
# #TE_MOD#    CONFIG.PCW_CPU_PERIPHERAL_DIVISOR0 {2} \
# #TE_MOD#    CONFIG.PCW_CPU_CPU_PLL_FREQMHZ {1333.333} \
# #TE_MOD#    CONFIG.PCW_CLK3_FREQ {10000000} \
# #TE_MOD#    CONFIG.PCW_CLK2_FREQ {10000000} \
# #TE_MOD#    CONFIG.PCW_CLK1_FREQ {200000000} \
# #TE_MOD#    CONFIG.PCW_CLK0_FREQ {100000000} \
# #TE_MOD#    CONFIG.PCW_CAN_PERIPHERAL_DIVISOR1 {1} \
# #TE_MOD#    CONFIG.PCW_CAN_PERIPHERAL_DIVISOR0 {1} \
# #TE_MOD#    CONFIG.PCW_ARMPLL_CTRL_FBDIV {40} \
# #TE_MOD#    CONFIG.PCW_ACT_WDT_PERIPHERAL_FREQMHZ {111.111115} \
# #TE_MOD#    CONFIG.PCW_ACT_UART_PERIPHERAL_FREQMHZ {100.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_TTC1_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
# #TE_MOD#    CONFIG.PCW_ACT_TTC1_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
# #TE_MOD#    CONFIG.PCW_ACT_TTC1_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
# #TE_MOD#    CONFIG.PCW_ACT_TTC0_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
# #TE_MOD#    CONFIG.PCW_ACT_TTC0_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
# #TE_MOD#    CONFIG.PCW_ACT_TTC0_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
# #TE_MOD#    CONFIG.PCW_ACT_TPIU_PERIPHERAL_FREQMHZ {200.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_SPI_PERIPHERAL_FREQMHZ {10.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_SMC_PERIPHERAL_FREQMHZ {10.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_SDIO_PERIPHERAL_FREQMHZ {100.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_QSPI_PERIPHERAL_FREQMHZ {200.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_PCAP_PERIPHERAL_FREQMHZ {200.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_FPGA3_PERIPHERAL_FREQMHZ {10.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_FPGA2_PERIPHERAL_FREQMHZ {10.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_FPGA1_PERIPHERAL_FREQMHZ {200.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_FPGA0_PERIPHERAL_FREQMHZ {100.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_ENET1_PERIPHERAL_FREQMHZ {10.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_ENET0_PERIPHERAL_FREQMHZ {10.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_DCI_PERIPHERAL_FREQMHZ {10.158730} \
# #TE_MOD#    CONFIG.PCW_ACT_CAN_PERIPHERAL_FREQMHZ {10.000000} \
# #TE_MOD#    CONFIG.PCW_ACT_APU_PERIPHERAL_FREQMHZ {666.666687} \
# #TE_MOD# #Empty Line
  # Create instance: ps7_0_axi_periph, and set properties
  set ps7_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect ps7_0_axi_periph ]
  set_property -dict [ list \
   CONFIG.NUM_MI {4} \
 ] $ps7_0_axi_periph

  # Create instance: rst_ps7_0_100M, and set properties
  set rst_ps7_0_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset rst_ps7_0_100M ]

  # Create instance: video_in
  create_hier_cell_video_in [current_bd_instance .] video_in

  # Create instance: video_out
  create_hier_cell_video_out [current_bd_instance .] video_out

  # Create instance: vio_0, and set properties
  set vio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:vio vio_0 ]
  set_property -dict [ list \
   CONFIG.C_PROBE_OUT0_INIT_VAL {0x1} \
 ] $vio_0

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {2} \
 ] $xlconcat_0

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {23} \
   CONFIG.DIN_TO {23} \
   CONFIG.DIN_WIDTH {24} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_0

  # Create interface connections
  connect_bd_intf_net -intf_net axi_gpio_0_GPIO [get_bd_intf_ports GPIO] [get_bd_intf_pins axi_gpio_0/GPIO]
# #TE_MOD#   connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins processing_system7_0/DDR]
# #TE_MOD#   connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
  connect_bd_intf_net -intf_net processing_system7_0_IIC_0 [get_bd_intf_ports IIC_A] [get_bd_intf_pins processing_system7_0/IIC_0]
  connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins processing_system7_0/M_AXI_GP0] [get_bd_intf_pins ps7_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M00_AXI [get_bd_intf_pins ps7_0_axi_periph/M00_AXI] [get_bd_intf_pins video_out/S_AXI_LITE]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M01_AXI [get_bd_intf_pins axi_gpio_0/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M01_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M02_AXI [get_bd_intf_pins ps7_0_axi_periph/M02_AXI] [get_bd_intf_pins video_in/S_AXI_LITE]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M03_AXI [get_bd_intf_pins axi_gpio_1/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M03_AXI]
  connect_bd_intf_net -intf_net video_in_M00_AXI [get_bd_intf_pins processing_system7_0/S_AXI_HP1] [get_bd_intf_pins video_in/M00_AXI]
  connect_bd_intf_net -intf_net video_out_M00_AXI [get_bd_intf_pins processing_system7_0/S_AXI_HP0] [get_bd_intf_pins video_out/M00_AXI]

  # Create port connections
  connect_bd_net -net ARESETN_1 [get_bd_pins ps7_0_axi_periph/ARESETN] [get_bd_pins rst_ps7_0_100M/interconnect_aresetn] [get_bd_pins video_in/axi_int_aresetn] [get_bd_pins video_out/axi_int_aresetn]
  connect_bd_net -net HPD_A_1 [get_bd_ports HPD_A] [get_bd_pins vio_0/probe_in0]
  connect_bd_net -net LOW_dout [get_bd_ports CEC_A] [get_bd_pins LOW/dout]
  connect_bd_net -net axi_gpio_1_gpio_io_o [get_bd_pins axi_gpio_1/gpio_io_o] [get_bd_pins video_in/control]
  connect_bd_net -net c_counter_binary_0_Q [get_bd_pins c_counter_binary_0/Q] [get_bd_pins xlslice_0/Din]
  connect_bd_net -net clk_rxn_0_1 [get_bd_ports CSI_C_N] [get_bd_pins video_in/clk_rxn_0]
  connect_bd_net -net clk_rxp_0_1 [get_bd_ports CSI_C_P] [get_bd_pins video_in/clk_rxp_0]
  connect_bd_net -net csi_d_n_0_1 [get_bd_ports CSI_D_N] [get_bd_pins video_in/csi_d_n]
  connect_bd_net -net csi_d_p_0_1 [get_bd_ports CSI_D_P] [get_bd_pins video_in/csi_d_p]
  connect_bd_net -net lp_n_0_1 [get_bd_ports CLP_D_N] [get_bd_pins video_in/lp_n]
  connect_bd_net -net lp_p_0_1 [get_bd_ports CLP_D_P] [get_bd_pins video_in/lp_p]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins axi_gpio_0/s_axi_aclk] [get_bd_pins axi_gpio_1/s_axi_aclk] [get_bd_pins c_counter_binary_0/CLK] [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK] [get_bd_pins processing_system7_0/S_AXI_HP1_ACLK] [get_bd_pins ps7_0_axi_periph/ACLK] [get_bd_pins ps7_0_axi_periph/M00_ACLK] [get_bd_pins ps7_0_axi_periph/M01_ACLK] [get_bd_pins ps7_0_axi_periph/M02_ACLK] [get_bd_pins ps7_0_axi_periph/M03_ACLK] [get_bd_pins ps7_0_axi_periph/S00_ACLK] [get_bd_pins rst_ps7_0_100M/slowest_sync_clk] [get_bd_pins video_in/axi_aclk] [get_bd_pins video_in/processing_clk] [get_bd_pins video_out/s_axi_lite_aclk] [get_bd_pins vio_0/clk]
  connect_bd_net -net processing_system7_0_FCLK_CLK1 [get_bd_pins processing_system7_0/FCLK_CLK1] [get_bd_pins video_in/ref_clk]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_ps7_0_100M/ext_reset_in] [get_bd_pins video_in/ext_resetn]
  connect_bd_net -net rst_ps7_0_100M_peripheral_aresetn [get_bd_pins axi_gpio_0/s_axi_aresetn] [get_bd_pins axi_gpio_1/s_axi_aresetn] [get_bd_pins ps7_0_axi_periph/M00_ARESETN] [get_bd_pins ps7_0_axi_periph/M01_ARESETN] [get_bd_pins ps7_0_axi_periph/S00_ARESETN] [get_bd_pins rst_ps7_0_100M/peripheral_aresetn] [get_bd_pins video_in/axi_aresetn] [get_bd_pins video_out/axi_resetn]
  connect_bd_net -net rst_ps7_0_100M_peripheral_reset [get_bd_pins rst_ps7_0_100M/peripheral_reset] [get_bd_pins video_out/vid_io_out_reset]
  connect_bd_net -net video_in_dma_int [get_bd_pins video_in/dma_int] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net video_out_hdmi_clk_n_0 [get_bd_ports HDMI_TXC_N] [get_bd_pins video_out/hdmi_clk_n]
  connect_bd_net -net video_out_hdmi_clk_p_0 [get_bd_ports HDMI_TXC_P] [get_bd_pins video_out/hdmi_clk_p]
  connect_bd_net -net video_out_hdmi_data_n_0 [get_bd_ports HDMI_TX_N] [get_bd_pins video_out/hdmi_data_n]
  connect_bd_net -net video_out_hdmi_data_p_0 [get_bd_ports HDMI_TX_P] [get_bd_pins video_out/hdmi_data_p]
  connect_bd_net -net video_out_mm2s_introut [get_bd_pins video_out/mm2s_introut] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net vio_0_probe_out0 [get_bd_ports CT_HPD] [get_bd_pins vio_0/probe_out0]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins processing_system7_0/IRQ_F2P] [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlslice_0_Dout [get_bd_ports GLED] [get_bd_pins xlslice_0/Dout]

  # Create address segments
  assign_bd_address -offset 0x41200000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_gpio_0/S_AXI/Reg] -force
  assign_bd_address -offset 0x41210000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_gpio_1/S_AXI/Reg] -force
  assign_bd_address -offset 0x43000000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs video_out/axi_vdma_0/S_AXI_LITE/Reg] -force
  assign_bd_address -offset 0x43010000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs video_in/axi_vdma_0/S_AXI_LITE/Reg] -force
  assign_bd_address -offset 0x00000000 -range 0x20000000 -target_address_space [get_bd_addr_spaces video_in/axi_vdma_0/Data_S2MM] [get_bd_addr_segs processing_system7_0/S_AXI_HP1/HP1_DDR_LOWOCM] -force
  assign_bd_address -offset 0x00000000 -range 0x20000000 -target_address_space [get_bd_addr_spaces video_out/axi_vdma_0/Data_MM2S] [get_bd_addr_segs processing_system7_0/S_AXI_HP0/HP0_DDR_LOWOCM] -force


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""



