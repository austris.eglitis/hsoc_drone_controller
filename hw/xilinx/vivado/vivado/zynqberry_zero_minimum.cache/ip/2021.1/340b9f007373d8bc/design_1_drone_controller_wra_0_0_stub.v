// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.1 (lin64) Build 3247384 Thu Jun 10 19:36:07 MDT 2021
// Date        : Wed Mar 16 21:26:24 2022
// Host        : archvb running 64-bit unknown
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_drone_controller_wra_0_0_stub.v
// Design      : design_1_drone_controller_wra_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg225-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "drone_controller_wraper,Vivado 2021.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(aclk, areset, axi_awvalid, axi_awready, 
  axi_awaddr, axi_wvalid, axi_wready, axi_wdata, axi_wstrb, axi_bvalid, axi_bready, axi_bresp, 
  axi_arvalid, axi_arready, axi_araddr, axi_rvalid, axi_rready, axi_rdata, axi_rresp, esc_fl, 
  esc_fr, esc_bl, esc_br)
/* synthesis syn_black_box black_box_pad_pin="aclk,areset,axi_awvalid,axi_awready,axi_awaddr[5:0],axi_wvalid,axi_wready,axi_wdata[31:0],axi_wstrb[3:0],axi_bvalid,axi_bready,axi_bresp[1:0],axi_arvalid,axi_arready,axi_araddr[5:0],axi_rvalid,axi_rready,axi_rdata[31:0],axi_rresp[1:0],esc_fl,esc_fr,esc_bl,esc_br" */;
  input aclk;
  input areset;
  input axi_awvalid;
  output axi_awready;
  input [5:0]axi_awaddr;
  input axi_wvalid;
  output axi_wready;
  input [31:0]axi_wdata;
  input [3:0]axi_wstrb;
  output axi_bvalid;
  input axi_bready;
  output [1:0]axi_bresp;
  input axi_arvalid;
  output axi_arready;
  input [5:0]axi_araddr;
  output axi_rvalid;
  input axi_rready;
  output [31:0]axi_rdata;
  output [1:0]axi_rresp;
  output esc_fl;
  output esc_fr;
  output esc_bl;
  output esc_br;
endmodule
