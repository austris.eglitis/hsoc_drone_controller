-- Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2021.1 (lin64) Build 3247384 Thu Jun 10 19:36:07 MDT 2021
-- Date        : Wed Mar 16 21:26:24 2022
-- Host        : archvb running 64-bit unknown
-- Command     : write_vhdl -force -mode synth_stub
--               /home/austris/EDI_prakse/hsoc_drone_controller/hw/xilinx/vivado/vivado/zynqberry_zero_minimum.gen/sources_1/bd/design_1/ip/design_1_drone_controller_wra_0_0/design_1_drone_controller_wra_0_0_stub.vhdl
-- Design      : design_1_drone_controller_wra_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg225-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_drone_controller_wra_0_0 is
  Port ( 
    aclk : in STD_LOGIC;
    areset : in STD_LOGIC;
    axi_awvalid : in STD_LOGIC;
    axi_awready : out STD_LOGIC;
    axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axi_wvalid : in STD_LOGIC;
    axi_wready : out STD_LOGIC;
    axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_bvalid : out STD_LOGIC;
    axi_bready : in STD_LOGIC;
    axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_arvalid : in STD_LOGIC;
    axi_arready : out STD_LOGIC;
    axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axi_rvalid : out STD_LOGIC;
    axi_rready : in STD_LOGIC;
    axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    esc_fl : out STD_LOGIC;
    esc_fr : out STD_LOGIC;
    esc_bl : out STD_LOGIC;
    esc_br : out STD_LOGIC
  );

end design_1_drone_controller_wra_0_0;

architecture stub of design_1_drone_controller_wra_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "aclk,areset,axi_awvalid,axi_awready,axi_awaddr[5:0],axi_wvalid,axi_wready,axi_wdata[31:0],axi_wstrb[3:0],axi_bvalid,axi_bready,axi_bresp[1:0],axi_arvalid,axi_arready,axi_araddr[5:0],axi_rvalid,axi_rready,axi_rdata[31:0],axi_rresp[1:0],esc_fl,esc_fr,esc_bl,esc_br";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "drone_controller_wraper,Vivado 2021.1";
begin
end;
