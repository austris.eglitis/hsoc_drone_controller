set_property PACKAGE_PIN L15 [get_ports esc_fl]
set_property IOSTANDARD LVCMOS33 [get_ports esc_fl]

set_property PACKAGE_PIN M15 [get_ports esc_fr]
set_property IOSTANDARD LVCMOS33 [get_ports esc_fr]

set_property PACKAGE_PIN P10 [get_ports esc_bl]
set_property IOSTANDARD LVCMOS33 [get_ports esc_bl]

set_property PACKAGE_PIN P15 [get_ports esc_br]
set_property IOSTANDARD LVCMOS33 [get_ports esc_br]
