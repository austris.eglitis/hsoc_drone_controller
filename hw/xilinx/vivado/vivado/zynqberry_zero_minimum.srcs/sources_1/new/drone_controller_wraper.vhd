library ieee;
use ieee.std_logic_1164.all;


entity drone_controller_wraper is
  generic(
    BIT_PERIOD_CLOCK_COUNT : natural := 1333
  );
  port(
    aclk    : in std_logic;
    areset  : in std_logic;

    -- memory mapped interface
    axi_awvalid : in  std_logic;
    axi_awready : out std_logic;
    axi_awaddr  : in  std_logic_vector(5 downto 0);
    axi_wvalid  : in  std_logic;
    axi_wready  : out std_logic;
    axi_wdata   : in  std_logic_vector(31 downto 0);
    axi_wstrb   : in  std_logic_vector(3 downto 0);
    axi_bvalid  : out std_logic;
    axi_bready  : in  std_logic;
    axi_bresp   : out std_logic_vector(1 downto 0);
    axi_arvalid : in  std_logic;
    axi_arready : out std_logic;
    axi_araddr  : in  std_logic_vector(5 downto 0);
    axi_rvalid  : out std_logic;
    axi_rready  : in  std_logic;
    axi_rdata   : out std_logic_vector(31 downto 0);
    axi_rresp   : out std_logic_vector(1 downto 0);

    -- memory mapped interface
    esc_fl : out std_logic;
    esc_fr : out std_logic;
    esc_bl : out std_logic;
    esc_br : out std_logic
  );
end entity;

architecture Behavioral of drone_controller_wraper is

begin

DRONE_CTRL : entity work.drone_controller
generic map (
    BIT_PERIOD_CLOCK_COUNT => BIT_PERIOD_CLOCK_COUNT
  )
  
port map(
    i_aclk    => aclk,
    i_areset  => areset,

    -- memory mapped interface
    i_axi_awvalid => axi_awvalid,
    o_axi_awready => axi_awready,
    i_axi_awaddr  => axi_awaddr, 
    i_axi_wvalid  => axi_wvalid,
    o_axi_wready  => axi_wready,
    i_axi_wdata   => axi_wdata,
    i_axi_wstrb   => axi_wstrb,
    o_axi_bvalid  => axi_bvalid,
    i_axi_bready  => axi_bready,
    o_axi_bresp   => axi_bresp, 
    i_axi_arvalid => axi_arvalid,
    o_axi_arready => axi_arready,
    i_axi_araddr  => axi_araddr,
    o_axi_rvalid  => axi_rvalid,
    i_axi_rready  => axi_rready,
    o_axi_rdata   => axi_rdata,
    o_axi_rresp   => axi_rresp,

    -- memory mapped interface
    o_esc_fl => esc_fl,
    o_esc_fr => esc_fr,
    o_esc_bl => esc_bl,
    o_esc_br => esc_br
);

end Behavioral;
