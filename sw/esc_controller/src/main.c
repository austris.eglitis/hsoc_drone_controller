#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#define _I(fmt, args...)   printf(fmt "\n", ##args)
#define _SE(fmt, args...)  printf("ERROR (%s): " fmt "\n", strerror(errno), ##args)

#define MAPPING_ADDRESS  0x43C00000
#define MAPPING_SIZE     4096
#define OFFSET_ENABLE    0x8
#define OFFSET_FRAME     0x0
#define OFFSET_DELAY     0x4


typedef struct {
  uint32_t command;
  uint32_t enable;
  uint32_t delay;
  uint32_t* addr;
  /* relevant data: current command, enabled state, delay, offset address (or maybe base address pointer) */
} esc_t;

esc_t* esc_init(uint32_t* addr){
esc_t* esc = (esc_t*)malloc(sizeof(esc_t));
  if (esc == NULL){
    _I("Memory not allocated");
    return NULL;
  }
  
  esc->addr    = addr;
  esc->command = 0;
  esc->enable  = 0;
  esc->delay   = 21;

  return esc;
}

void esc_deinit(esc_t *esc){
  free(esc);
}

int esc_set_enable(esc_t *esc, uint16_t value){
  *(uint16_t*)((uint32_t)esc->addr + OFFSET_ENABLE) = value;
}

int esc_set_command(esc_t *esc, uint16_t value){
  
  value           = value << 1;
  value           &= 0xffff;
  uint16_t crc    = (value ^ (value >> 4) ^ (value >> 8)) & 0x0F;
  value           = (value << 4) | crc;

  *(uint16_t*)((uint32_t)esc->addr + OFFSET_FRAME) = value;
}

int esc_set_delay(esc_t *esc, uint16_t value){
  *(uint16_t*)((uint32_t)esc->addr + OFFSET_DELAY) = value;
}



static inline void dshot_set_enable(uint16_t *addr, uint16_t value){
  *(uint16_t*)((uint32_t)addr + OFFSET_ENABLE) = value;
}

static inline void dshot_set_frame(uint16_t *addr, uint16_t value){
  *(uint16_t*)((uint32_t)addr + OFFSET_FRAME) = value;
}

static inline void dshot_set_delay(uint16_t *addr, uint16_t value){
  *(uint16_t*)((uint32_t)addr + OFFSET_DELAY) = value;
}

static inline void print_frame(uint16_t frame){
  _I("data: 0x%.4x; telemetry: %d; crc: 0x%.1x (%s=%d)",
    (frame >> 5), ((frame >> 4) & 0x1), (frame & 0xf),
    ((frame >> 5) < 48) ? "COMMAND" : "THROTTLE", (frame >> 5));
}



int main(int argc, const char *argv[]){
  int fd;
  uint32_t *sender;
  uint16_t  data;
  uint16_t  delay;
  uint16_t  enable;
  uint16_t  crc;
  uint16_t  motorID;

  // input interface:
  if(argc != 4){
    _I("USAGE:");
    _I("%s <data> <delay> <enable> <motorID>", argv[0]);
    _I("Motor ID: \t 0 = All motors");
    _I("\t\t 1 = Front Left  (FL)");
    _I("\t\t 2 = Front Right (FR)");
    _I("\t\t 3 = Back Left   (BL)");
    _I("\t\t 4 = Back Right  (BR)");
  }

  data    = atoi(argv[1]);
  delay   = atoi(argv[2]);
  enable  = atoi(argv[3]);
  motorID = atoi(argv[4]); 

  _I("Retreiving '/dev/mem' file descriptor");
  fd = open("/dev/mem", O_RDWR|O_SYNC);
  if(fd == -1){
    _SE("Failed to open file! LoL, noob!");
    return 1;
  }

  _I("Requesting physical memory mapping");
  sender = mmap(NULL, MAPPING_SIZE, PROT_WRITE|PROT_READ, MAP_SHARED, fd, MAPPING_ADDRESS);
  if(sender == MAP_FAILED){
    _SE("Failed to map address! LoL, noob!");
    close(fd);
    return 1;
  }

  _I("Data is: %u",    data);
  _I("Delay is: %u",   delay);
  _I("Enable is: %u",  enable);
  _I("MotorID is: %u", motorID);
  
  //motor cmnds
  if(motorID == 0){

    //ESC init 
    esc_t* esc_fl = esc_init(sender);
    esc_t* esc_fr = esc_init(sender+3); 
    esc_t* esc_bl = esc_init(sender+6); 
    esc_t* esc_br = esc_init(sender+9); 
  

    //motor settings
    esc_set_command(esc_fl, data);
    esc_set_command(esc_fr, data);
    esc_set_command(esc_bl, data);
    esc_set_command(esc_br, data);

    esc_set_delay(esc_fl, delay);
    esc_set_delay(esc_fr, delay);
    esc_set_delay(esc_bl, delay);
    esc_set_delay(esc_br, delay);
  
    esc_set_enable(esc_fl, enable);
    esc_set_enable(esc_fr, enable);
    esc_set_enable(esc_bl, enable);
    esc_set_enable(esc_br, enable);


    //ESC deinit
    esc_deinit(esc_fl);
    esc_deinit(esc_fr);
    esc_deinit(esc_bl);
    esc_deinit(esc_br);
  }
  //front left motor
  else if(motorID == 1){
    esc_t* esc_fl = esc_init(sender);
    esc_set_command(esc_fl, data);
    esc_set_delay(esc_fl, delay);
    esc_set_enable(esc_fl, enable);
    esc_deinit(esc_fl);
  }
  //front right motor
  else if(motorID == 2){
    esc_t* esc_fr = esc_init(sender+3);
    esc_set_command(esc_fr, data);
    esc_set_delay(esc_fr, delay);
    esc_set_enable(esc_fr, enable);
    esc_deinit(esc_fr);
  }
  // back left motor
  else if(motorID == 3){
    esc_t* esc_bl = esc_init(sender+6);
    esc_set_command(esc_bl, data);
    esc_set_delay(esc_bl, delay);
    esc_set_enable(esc_bl, enable);
    esc_deinit(esc_bl);
  }
  // back right motor
  else if(motorID == 4){
    esc_t* esc_br = esc_init(sender+9);
    esc_set_command(esc_br, data);
    esc_set_delay(esc_br, delay);
    esc_set_enable(esc_br, enable);
    esc_deinit(esc_br);
  }
  else
    _I("Invalid motor ID provided");
 

  _I("Releasing physical memory mapping");
  if(munmap(sender, MAPPING_SIZE) == -1){
    _SE("Failed to unmap address space! LoL, noob!");
    close(fd);
  }

  _I("Releasing '/dev/mem' file descriptor");
  close(fd);

  return 0;
}
