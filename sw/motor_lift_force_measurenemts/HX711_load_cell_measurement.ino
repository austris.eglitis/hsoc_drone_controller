#include "HX711.h"

#define calibration_factor -235.0 // kalibracijas koeficients masas uzradisanai gramos

#define DOUT  3
#define CLK  2

HX711 scale;

void setup() {
  Serial.begin(9600);
  Serial.println("HX711 scale demo");

  scale.begin(DOUT, CLK);
  scale.set_scale(calibration_factor); 
  scale.tare(); 
  Serial.println("Readings:");
}

void loop() {
  Serial.print(scale.get_units(), 1); //scale.get_units() returns a float
  Serial.print(" g;"); 
  Serial.println();
}
