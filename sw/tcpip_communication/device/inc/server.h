#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdint.h>

// handler declarations
int handler_cmd_arm     (void *data, uint32_t size);
int handler_cmd_disarm  (void *data, uint32_t size);
int handler_cmd_liftoff (void *data, uint32_t size);
int handler_cmd_land    (void *data, uint32_t size);

//other declarations
void error(char *mgs);
void hexdump(void *buffer, unsigned size);
int server_init(uint16_t port);
void server_deinit(int sockfd);
int server_run(int sockfd);

#endif
