/* SERVER */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "server.h"


int main(){
    int sockfd;

    if ((sockfd = server_init(8080)) == -1){
        printf("Failed to intialize server\n");
        exit(1);
    }
    
    if ((server_run(sockfd)) == -1){
        printf("Failed to run server\n");
    }

    server_deinit(sockfd);
    printf("Socked closed\n");
    return 0;
}
