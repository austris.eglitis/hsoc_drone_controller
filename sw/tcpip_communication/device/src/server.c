#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "server.h"

#define BUFSIZE 256 


/* Common with client */
typedef enum{
    PACKET_CMD_ARM,
    PACKET_CMD_DISARM,
    PACKET_CMD_LIFTOFF,
    PACKET_CMD_LAND
} packetType_t;

typedef struct{
    packetType_t type;
    uint32_t     size;
    uint8_t      data[32];
} packet_t;


/* Forward declarations of handlers */
int handler_cmd_arm     (void *data, uint32_t size);
int handler_cmd_disarm  (void *data, uint32_t size);
int handler_cmd_liftoff (void *data, uint32_t size);
int handler_cmd_land    (void *data, uint32_t size);

/* Array of handlers */
int (*handlers[])(void *data, uint32_t size) = {
    handler_cmd_arm,
    handler_cmd_disarm,
    handler_cmd_liftoff,
    handler_cmd_land
};

/* Handler functions */
int handler_cmd_arm(void *data, uint32_t size){
    printf("This is cmd_arm handler func. Size is %d\n",size);
    printf("Recieved data: in handler func %s", (char*)data); 
}

int handler_cmd_disarm(void *data, uint32_t size){
    printf("This is cmd_disarm handler\n");
}

int handler_cmd_liftoff(void *data, uint32_t size){
    printf("This is cmd_lifoff handler\n");
}

int handler_cmd_land(void *data, uint32_t size){
    printf("This is cmd_land handler\n");
}



void error(char *msg){
    perror(msg);
}

void hexdump(void *buffer, unsigned size){
    printf("Buffer hexdump: \n");

    unsigned char *ptr = (char*)buffer; 
    for(int i=0; i<size; i++){
        printf("%x\t",ptr[i]);
    }
    printf("\n\n");
}

int server_init(uint16_t port){
    int sockfd;
    struct sockaddr_in serv_addr;
    
    // initialize socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1){
        error("Error creating socket");
        return -1;
    }
    else 
        printf("Socket created\n");


    // set up address
    bzero((char*)&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);

    // bind the socket
    int ret = bind(sockfd, (struct sockaddr*) &serv_addr,sizeof(serv_addr));
    if (ret == -1){
        error("Error while binding");
        close(sockfd);
        return -1;
    }  

    return sockfd;
}

void server_deinit(int sockfd){
    close(sockfd);
}

int server_run(int sockfd){
    struct sockaddr_in serv_addr, cli_addr;
    int clilen, newsockfd, n;
    char buffer[BUFSIZE];

    // enter listen mode
    listen(sockfd,5); // 2nd argument is max clients on server
    clilen = sizeof(cli_addr);

    newsockfd = accept(sockfd,(struct sockaddr*) &cli_addr, &clilen);
    if (newsockfd<0){
        error("Error while listening\n");
        return -1;
    }
    else
        printf("Socket connection established\n");
    
    bzero(buffer,BUFSIZE); //clearing buffer
    n = read(newsockfd,buffer,BUFSIZE); // reading from buffer
    if (n == -1){
        error("Error reading from socket");
        return -1;
    }
    
    //printf("Size of received data is: %d\n",sizeof(buffer));
    //hexdump(buffer, BUFSIZE);    
     
    // gets the struct from buffer 
    //packet_t *ptr; 
    packet_t *packet_data;
    packet_data = (packet_t*)&buffer; 
    //packet_data = *ptr;

    // test check if data is recieved    
    printf("packet_data->type: %d\n",   packet_data->type);
    printf("packet_data->size: %d\n",   packet_data->size); 
    printf("packet_data->data: %s\n\n", packet_data->data);
   
    // calls a handler function 
    handlers[packet_data->type](packet_data->data, packet_data->size); 
    printf("\n");
    

    // sends back message to client
    n = write(newsockfd,"Message recieved\n",18); //18 is char count in msg
    if (n == -1){
        error("Error writing to socket");
        return -1;
    }

    return 0;
}


