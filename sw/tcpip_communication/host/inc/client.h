#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <stdint.h>

//delcarations
void error(char *msg);
int client_init(uint16_t port, char *ip);
int client_send(int sockfd);
void client_deinit(int sockfd);

#endif
