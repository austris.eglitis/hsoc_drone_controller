#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "client.h"

#define BUFSIZE 256 


/* Common with server */
typedef enum{
    PACKET_CMD_ARM,
    PACKET_CMD_DISARM,
    PACKET_CMD_LIFTOFF,
    PACKET_CMD_LAND
} packetType_t;

typedef struct{
    packetType_t type;
    uint32_t     size;
    uint8_t      data[32]; //has fixed size for now
} packet_t;

//can fill out a struct with test data and change packet_t type
packet_t packet_data = {PACKET_CMD_ARM,sizeof(packet_t),"say hello for test"};


void error(char *msg){
    perror(msg);
}

int client_init(uint16_t port, char *ip){
    int sockfd, connfd;
    struct sockaddr_in servaddr;
    
    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        error("Socket creation failed\n");
        return -1;
    }
    else
        printf("Socket created\n");
   
    bzero(&servaddr, sizeof(servaddr));
  
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);
    servaddr.sin_port = htons(port);
  
    // connect the client socket to server socket
    int ret = connect(sockfd, (struct sockaddr*) &servaddr, sizeof(servaddr));
    if (ret != 0) {
        error("Connection with the server failed\n");
        return -1;
    }
    else
        printf("Socket connection established\n");

    return sockfd;
}

int client_send(int sockfd){
    char buffer[BUFSIZE];
    int n;
    
    // cleans the buffer
    bzero(buffer,BUFSIZE);
    
    //cmd_t ptr = CMD_LIFTOFF; // for enum test 
       
    printf("Size of packet_t is: %lu\n", sizeof(packet_t)); 
    memcpy(buffer, &packet_data, sizeof(packet_t));

    n = write(sockfd,buffer,BUFSIZE);
    if (n == -1){
        error("Error writing to socket");
        return -1;
    }
    else
        printf("Data sent succesfully\n\n");
         
     bzero(buffer,BUFSIZE);
     n = read(sockfd,buffer,BUFSIZE);
     if (n == -1){
        error("Error reading from socket");
        return -1;
     }
        
     return 0;
}

void client_deinit(int sockfd){
    close(sockfd);
}

