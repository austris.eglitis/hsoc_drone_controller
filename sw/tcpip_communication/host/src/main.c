/* CLIENT */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "client.h"

// TODO: default and generic IP address
// TODO: default and generic port
// https://stackoverflow.com/questions/9642732/parsing-command-line-arguments-in-c
int main(int argc, char *argv[]){
    int sockfd;
    char ip[] = "127.0.0.1";

    sockfd = client_init(8080,ip);
    if (sockfd == -1){
        printf("Failed to initialize client\n");
        exit(1);
    }

    if (client_send(sockfd) == -1){
        printf("Failed to send data\n");
        exit(1);
    }

    client_deinit(sockfd);
    printf("Socket closed\n");
    return 0;
}
